#pragma once

#include "Engine/global.h"

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	int _hPict;    //画像番号
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};