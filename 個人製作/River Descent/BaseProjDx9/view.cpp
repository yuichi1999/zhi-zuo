#include "view.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Model.h"
#include "Player.h"
#include "Playscene.h"

//コンストラクタ
view::view(IGameObject * parent)
	:IGameObject(parent, "view"), preHp(3),_hModel(-1),Hp(3),frame(0)
	, _pText(nullptr), _pText2(nullptr), _pText3(nullptr),
	frame_(0), cnt1_(0), cnt10_(0), minCnt_(MIN_COUNT),TimeFlag(FALSE),TimeCnt(FALSE)
{
}

//デストラクタ
view::~view()
{
}

//初期化
void view::Initialize()
{
	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 6, -9));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));

	//モデルデータのロード
	_hModel = Model::Load("data/uf.fbx");
	assert(_hModel >= 0);

	std::string fileName[4] =
	{
		"Data/pict/life0.png",	//0
		"Data/pict/life1.png",	//1
		"Data/pict/life2.png",	//2
		"Data/pict/life3.png",	//3
	};

	//登録した画像を読み込み
	for (int i = 0; i <= 3; i++)
	{
		_hImage[i] = Image::Load(fileName[i]);
	}

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 0.7f);
	AddCollider(collision);

	// テキストを作成
	_pText = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText2 = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText3 = new Text("ＭＳ ゴシック", 60);

	InitTime();

}

//更新
void view::Update()
{	
	//プレイヤークラスのhpを参照、比較
	if (preHp > ((Player*)GetParent())->hp)
	{
		Hp--;
		preHp--;
	}

	//Hpが0で1秒後に
	if (Hp == 0)
	{
		frame++;
		if (frame == 90)
		{
			//ゲームオーバーシーンへ
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
		}
	}

	if(TimeCnt == FALSE)
	{ 
		TimeCount();
	}
	if (TimeCnt == TRUE)
	{
		TimeFlag = TRUE;
		frame++;
		if (frame == 90)
		{
			//ゲームオーバーシーンへ
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
		}
	}
	//if (preHp > ((PlayScene*)GetParent())->GetPlayer()->hp)
}

//描画
void view::Draw()
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 50, -100, 0);

	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 0.5f, 0.5f, 0.5f);

	Image::SetMatrix(_hImage[Hp], localMatrix_ * m * a);
	Image::Draw(_hImage[Hp]);

	//Model::SetMatrix(_hModel, worldMatrix_);
	//Model::Draw(_hModel);

	_pText->Draw(1200, 50, sec_[cnt1_]); //自動で299にする
	_pText2->Draw(1160, 50, sec_[cnt10_]); //自動で299にする
	_pText3->Draw(1120, 50, min_[minCnt_]); //自動で299にする
}

//開放
void view::Release()
{
	delete _pText;
	delete _pText2;
	delete _pText3;
}

void view::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "Obstacle")
	{
		//ゲームオーバーシーンへ
		//SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		//pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
		/*if ((_pPlayer->preHp) != hp)
		{
			hp--;
		}*/
		//_pPlayer->preHp = hp;
	}	

	/*if ((_pPlayer->preHp) != hp)
	{
		hp--;
	}*/
}

// 時間をセット
void view::InitTime()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

//タイムカウント
void view::TimeCount()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;

	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初500から499にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 1;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 1;
					cnt1_ = 1;
					minCnt_++;

				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

				//タイムアップ
				if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
				{
					TimeCnt = TRUE;
					
				}

			}
			/*if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
			{
			cnt1_ = 0;
			cnt10_ = 0;
			minCnt_ = 7;
			}*/
		}
	}
}