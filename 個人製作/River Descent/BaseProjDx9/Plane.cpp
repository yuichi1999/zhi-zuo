//#include "Plane.h"
//#include "Engine/ResouceManager/Model.h"
//
////コンストラクタ
//Plane::Plane(IGameObject * parent)
//	:IGameObject(parent, "Plane"), hModel_(-1), pEffect_(nullptr), pToontex_(nullptr)
//{
//}
//
////デストラクタ
//Plane::~Plane()
//{
//	SAFE_RELEASE(pToontex_);
//	SAFE_RELEASE(pEffect_);
//}
//
////初期化
//void Plane::Initialize()
//{
//	LPD3DXBUFFER err = 0;
//	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
//		"NormalMapShader.hlsl", NULL, NULL,
//		D3DXSHADER_DEBUG, NULL, &pEffect_,
//		&err)))
//	{
//		MessageBox(NULL,
//			(char*)err->GetBufferPointer(),
//			"シェーダーエラー", MB_OK);
//	}
//
//	
//	//モデルデータのロード
//	/*hModel_ = Model::Load("data/donut2.fbx", pEffect_);
//	assert(hModel_ >= 0);*/
//
//	hModel_ = Model::Load("data/normal.fbx", pEffect_);
//	assert(hModel_ >= 0);
//	
//
//
//	D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/heno.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
//		D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToontex_);
//
//	//pEffect_->SetTexture("TEXTURE_TOON", pToontex_);
//	//assert(pEffect_ >= 0);
//}
//
////更新
//void Plane::Update()
//{
//
//	/*static float count = 0.0f;
//	count += 0.1f;
//	scale_.x = sin(count) + 2;*/
//	
//
//	//rotate_.y += 1.0f;
//
//
//	/*scale_.x -= 0.05f;
//	scale_.y -= 0.05f;
//	scale_.z -= 0.05f;*/
//}
//
////描画
//void Plane::Draw()
//{
//	Model::SetMatrix(hModel_, worldMatrix_);
//
//	//ビュー行列
//	D3DXMATRIX view;
//	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);
//
//	//プロジェクション行列
//	D3DXMATRIX proj;
//	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);
//
//	D3DXMATRIX matWVP = worldMatrix_ * view * proj;
//
//	pEffect_->SetMatrix("WVP", &matWVP);
//
//	//pEffect_->SetMatrix("W", &worldMatrix_);
//
//	//	D3DXMATRIX mat = worldMatrix_;
//	//	mat._41 = 0;
//		//	mat._42 = 0;
//		//	mat._43 = 0;
//
//	//回転行列
//	D3DXMATRIX rotateX, rotateY, rotateZ;
//	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
//	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
//	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));
//
//	//拡大縮小
//	D3DXMATRIX scale;
//	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
//	D3DXMatrixInverse(&scale, nullptr, &scale);
//
//	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;
//
//	pEffect_->SetMatrix("RS", &mat);
//
//	//ライトの向きをシェーダに渡す
//	D3DLIGHT9 lightState;
//	Direct3D::pDevice_->GetLight(0, &lightState);
//
//	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);
//
//	//カメラの位置
//	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 2, -5));
//	pEffect_->SetMatrix("W", &worldMatrix_);
//
//	pEffect_->SetTexture("TEXTURE_TOON", pToontex_);
//
//	pEffect_->Begin(NULL, 0);
//	//pEffect_->BeginPass(0);
//	pEffect_->BeginPass(1);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
//
//	Model::Draw(hModel_);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
//	pEffect_->EndPass();
//
//	//普通に表示
//	pEffect_->BeginPass(0);
//	Model::Draw(hModel_);
//	pEffect_->EndPass();
//
//	//シェーダーを使った描画終わり。
//	//これ以降はDirectXデフォルトの描画に戻る
//
//	pEffect_->End();
//
//}
//
////開放
//void Plane::Release()
//{
//}