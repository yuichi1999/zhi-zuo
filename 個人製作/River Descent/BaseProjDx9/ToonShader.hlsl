//グローバル変数(アプリ側から返される情報)
float4x4 WVP;   //ワールド、ビュー、プロジェクション
float4x4 RS;     //回転行列と拡大の逆行列
float4x4 W;      //ワールド行列
float4 LIGHT_DIR;
float4 DIFFUSE_COLOR;
float4	 AMBIENT_COLOR;
float4 SPECULAR_COLOR;
float SPECULAR_POWER;
float4 CAMERA_POS; //視点(カメラの位置)
bool	 IS_TEXTURE;
texture TEXTURE;
texture TEXTURE_TOON;

//テクスチャを張るための設定
//サンプラー
sampler texSampler = sampler_state
{

	Texture = <TEXTURE>;
	MinFilter = LINEAR;//
	MagFilter = LINEAR;//テクスチャを滑らかにするか
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;

};

sampler toonSampler = sampler_state
{
	Texture = <TEXTURE_TOON>;
	AddressU = Clamp;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

//構造体
//頂点シェーダーの出力でピクセルシェーダの入力
struct VS_OUT
{
	float4 pos		:SV_POSITION;//位置
	float4 normal   :NORMAL;     //法線
	float4 eye      :TEXCOORD1;  //TEXCOORD←専用のセマンティクスがなければこれを入れる
	float2 uv       :TEXCOORD0;  //UV座標
};


//頂点シェーダ                                  法線
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{

	//出力シェーダ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);   //位置

	normal = mul(normal, RS);
	normal = normalize(normal);  //法線を正規化
	outData.normal = normal;

	float4 worldPos = mul(pos, W);//頂点の位置管理
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;

	//まとめて出力
	return outData;
}

//ピクセルシェーダ
float4 PS(VS_OUT inData) : COLOR
{
	inData.normal = normalize(inData.normal);
	inData.eye = normalize(inData.eye);
//仮のライト
float4 lightDir = LIGHT_DIR;
lightDir = normalize(lightDir);

//拡散反射光
float u = dot(inData.normal, -lightDir);
float4 diffuse = tex2D(toonSampler,float2(u,0));
diffuse.a = 1;
//diffuse *= DIFFUSE_COLOR; //色にしている
//diffuse *= tex2D(texSampler, inData.uv);

if (IS_TEXTURE == false)
{
	diffuse *= DIFFUSE_COLOR;
}
else
{
	diffuse *= tex2D(texSampler, inData.uv);
}


//環境光 
//float4 ambient = float4(0.2, 0.2, 0.2, 0); //色を加えている
float4 ambient = AMBIENT_COLOR;

//鏡面反射光
float4 R = reflect(lightDir, inData.normal); //反射光
float4 speculer = pow(dot(R, inData.eye), SPECULAR_POWER) * 2 * SPECULAR_COLOR;

//頂点シェーダで色を求めているので、
//return  ambient + diffuse + speculer;
return diffuse;
}

float4 VS_Toon(float4 pos : POSITION, float4 normal : NORMAL) : SV_POSITION
 {
	normal.w = 0;
	pos += normal / 20;
	pos = mul(pos, WVP);
	return pos;
 }

float4 PS_Toon(float4 pos : SV_POSITION) : COLOR
 {
	return float4(0, 0, 0, 1);
 }

//テクニック
technique
 {
	 pass
	 {

		 // シェーダは実行時にコンパイルする 
		 VertexShader = compile vs_3_0 VS();
		 PixelShader = compile ps_3_0 PS();

	 }

	 pass
	 {

		 // シェーダは実行時にコンパイルする 
		 VertexShader = compile vs_3_0 VS_Toon();
		 PixelShader = compile ps_3_0 PS_Toon();

	 }
 }

