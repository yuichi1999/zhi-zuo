#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "view.h"
#include "River.h"
#include "Stage.h"
#include "Playscene.h"


//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hModel_(-1), pEffect_(nullptr)
	, pRoughtex_(nullptr),pCubetex_(nullptr),hRiverModel_(-1),hp(3),speedA(1),speedD(1)
	,hStageModel_(-1),MoveFlag(FALSE)
	//pToontex_(nullptr) pCubetex_(nullptr)
{
}

//デストラクタ
Player::~Player()
{
	SAFE_RELEASE(pRoughtex_);
	//SAFE_RELEASE(pToontex_);
	SAFE_RELEASE(pEffect_);
	SAFE_RELEASE(pCubetex_);
}

//初期化
void Player::Initialize()
{

	_pview = CreateGameObject<view>(this);
	
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"NorEnvShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
	&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	

	//モデルデータのロード
	/*hModel_ = Model::Load("data/donut2.fbx", pEffect_);
	assert(hModel_ >= 0);*/


	//D3DXCreateCubeTextureFromFile(Direct3D::pDevice_, "data/Roughness.png", &pRoughtex_);

	hModel_ = Model::Load("data/uf.fbx", pEffect_);
	D3DXCreateCubeTextureFromFile(Direct3D::pDevice_, "data/CubeMap2.dds", &pCubetex_);
		D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/Roughness.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pRoughtex_);

	//アニメ調
	/*D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/UV.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
		D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToontex_);*/


		//Camera* pCamera = CreateGameObject<Camera>(this);

	//assert(pEffect_ >= 0);
		
		scale_ = D3DXVECTOR3(1.5f, 1.5f, 1.5f);

		//position_.z -= 50.0f;

		
		//当たり判定追加
		SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 0.7f);
		AddCollider(collision);
		
		/*if (position_.y <= 5.0)
		{
			rotate_.x += 15.0f;
		}*/

		position_.z -= 60.0f;
		
		position_.x -= 5.5f;
		//rotate_.x += 90.0f;
		

}

//更新
void Player::Update()
{
	FollowGround();

	//hp0で操作不能にする
	if (hp == 0)
	{
		MoveFlag = TRUE;
	}

	//hpがあるときは操作可能にする
	if (MoveFlag == FALSE )
	{
		Move();
	}

	if (_pview->TimeFlag == TRUE)
	{
		MoveFlag = TRUE;
	}

	if (hp < 0)
	{
		hp = 0;
	}
}

//描画
void Player::Draw()
{
	

	Model::SetMatrix(hModel_, worldMatrix_);
	
	//ビュー行列
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	pEffect_->SetMatrix("WVP", &matWVP);

	//pEffect_->SetMatrix("W", &worldMatrix_);

	//	D3DXMATRIX mat = worldMatrix_;
	//	mat._41 = 0;
		//	mat._42 = 0;
		//	mat._43 = 0;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));
	
			//拡大縮小
		D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);
	
	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;
	
	pEffect_->SetMatrix("RS", &mat);

		//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);

	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*) &D3DXVECTOR3(0, 2, -5));
	pEffect_->SetMatrix("W", &worldMatrix_);

	//pEffect_->SetTexture("TEXTURE_TOON", pToontex_);

	pEffect_->SetTexture("ROUGH_MAP", pRoughtex_);

	pEffect_->SetTexture("TEX_CUBE", pCubetex_);

	pEffect_->Begin(NULL, 0);
	//pEffect_->BeginPass(0);
	pEffect_->BeginPass(1);

	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	Model::Draw(hModel_);

	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pEffect_->EndPass();

	//普通に表示
	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();

	//シェーダーを使った描画終わり。
	//これ以降はDirectXデフォルトの描画に戻る
	
	pEffect_->End();
	

}

//開放
void Player::Release()
{
}

void Player::Move()
{


	/*static float count = 0.0f;
	count += 0.1f;
	scale_.x = sin(count) + 2;*/
	//rotate_.y += 1.0f;

	/*scale_.x -= 0.05f;
	scale_.y -= 0.05f;
	scale_.z -= 0.05f;*/


	//position_.z += 0.1f;	// 川下りなのでゆっくりと自動で流れる

	D3DXMATRIX move;
	D3DXMatrixRotationY(&move, D3DXToRadian(rotate_.y));

	//通常移動
	D3DXVECTOR3 UV = D3DXVECTOR3(0, 0, 0.1f);
	D3DXVec3TransformCoord(&UV, &UV, &move);

	D3DXVECTOR3 LR = D3DXVECTOR3(0.1f, 0, 0);
	D3DXVec3TransformCoord(&LR, &LR, &move);

	//加速用関数
	//speedが50を上回るまで加速
	if (Input::IsKey(DIK_A) && speedA <= 25)
	{
		speedA = speedA + 2;
	}
	//離していると減速
	else if (speedA > 1)
	{
		speedA--;
	}

	//加速用関数
	//speedが50を上回るまで加速
	if (Input::IsKey(DIK_D) && speedD <= 25)
	{
		speedD = speedD + 2;
	}
	//離していると減速
	else if (speedD > 1)
	{
		speedD--;
	}

	if (Input::IsKey(DIK_W) && speedW <= 25)
	{
		speedW = speedW + 2;
	}
	//離していると減速
	else if (speedW > 1)
	{
		speedW--;
	}

	if (Input::IsKey(DIK_S) && speedS <= 25)
	{
		speedS = speedS + 2;
	}
	//離していると減速
	else if (speedS > 1)
	{
		speedS--;
	}





	//動く前の位置設定
	D3DXVECTOR3 prePos = position_;

	//→が押されていたら
	if (Input::IsKey(DIK_D))
	{
		//右へ移動
		position_ += LR * 0.5 * (speedD / 10);
	}
	else
	{
		position_ += LR * 0.5 * ((speedD - 1) / 10);
	}

	//←が押されていたら
	if (Input::IsKey(DIK_A))
	{
		//左へ移動
		position_ -= LR * 0.5 * (speedA / 10);
	}
	else
	{
		position_ -= LR * 0.5 * ((speedA - 1) / 10);
	}

	//→が押されていたら
	if (Input::IsKey(DIK_UP))
	{
		//右へ移動
		position_ += UV * 0.5 * (speedW / 10);
	}
	else
	{
		position_ += UV * 0.5 * ((speedW - 1) / 10);
	}

	//←が押されていたら
	if (Input::IsKey(DIK_S))
	{
		//左へ移動
		position_ -= UV * 0.5 * (speedS / 10);
	}
	else
	{
		position_ -= UV * 0.5 * ((speedS - 1) / 10);
	}
}

void Player::OnCollision(IGameObject * pTarget)
{
	//障害物に当たった
	if (pTarget->GetObjectName() == "Obstacle")
	{
		hp--;
		
	}

	//旗に当たった
	if (pTarget->GetObjectName() == "Flag")
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR);
	}
}

//川に沿わせる
void Player::FollowGround()
{
	if (hStageModel_ == -1)
	{
		//モデル番号を調べる
		hStageModel_ = ((Stage*)FindObject("Stage"))->GetModelHandle();
	}

	//もう川のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data2;
		data2.start = position_;					//プレイヤーの原点から
		data2.start.y = 0;						//高さ0（川は一番高いところでもY<0になっている）
		data2.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//川に対してレイを撃つ
		Model::RayCast(hStageModel_, &data2);

		//レイが地面に当たったら
		if (data2.hit)
		{
			//プレイヤーの高さを川にあわせる
			//(Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//そこの標高は『-data.distメートル』ということになる)
			position_.y = -data2.dist;
			//position_.x = -3.0;
			//position_.z = -3.0;
		}
	}

	if (hRiverModel_ == -1)
	{
		//モデル番号を調べる
		hRiverModel_ = ((River*)FindObject("River"))->GetModelHandle();
	}

	//もう川のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data;
		data.start = position_;					//プレイヤーの原点から
		data.start.y = 0;						//高さ0（川は一番高いところでもY<0になっている）
		data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//川に対してレイを撃つ
		Model::RayCast(hRiverModel_, &data);

		//レイが地面に当たったら
		if (data.hit)
		{
			//プレイヤーの高さを川にあわせる
			//(Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//そこの標高は『-data.distメートル』ということになる)
			position_.y = -data.dist;
		}
	}

	
	
}
 