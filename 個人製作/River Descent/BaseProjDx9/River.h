#pragma once
#include "Engine/GameObject/GameObject.h"

//川を管理するクラス
class River : public IGameObject
{
	int hModel_;    //モデル番号
	
	LPD3DXEFFECT	pEffect_;

	LPDIRECT3DTEXTURE9	pToontex_;

public:
	//コンストラクタ
	River(IGameObject* parent);

	//デストラクタ
	~River();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//モデル番号のゲッター
	int GetModelHandle() { return hModel_; }
};