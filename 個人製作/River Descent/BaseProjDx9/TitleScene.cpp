#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), _hPict(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/pict/タイトル.png");
	assert(_hPict >= 0);

}

//更新
void TitleScene::Update()
{
	//position_.y--;
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void TitleScene::Release()
{
}