#include "Obstacle.h"
#include "Engine/ResouceManager/Model.h"
#include "River.h"
#include "Stage.h"

//コンストラクタ
Obstacle::Obstacle(IGameObject * parent)
	:IGameObject(parent, "Obstacle"), _hModel(-1), hRiverModel_(-1),
	Move(FALSE),pEffect_(nullptr)
	, pRoughtex_(nullptr), pCubetex_(nullptr),MoveFrag(FALSE),hStageModel_(-1)
{
}

//デストラクタ
Obstacle::~Obstacle()
{
	SAFE_RELEASE(pRoughtex_);
	//SAFE_RELEASE(pToontex_);
	SAFE_RELEASE(pEffect_);
	SAFE_RELEASE(pCubetex_);
}

//初期化
void Obstacle::Initialize()
{
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"NorEnvShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	//モデルデータのロード
	//_hModel = Model::Load("data/koura2.fbx");
	//assert(_hModel >= 0);
	_hModel = Model::Load("data/uf.fbx", pEffect_);
	D3DXCreateCubeTextureFromFile(Direct3D::pDevice_, "data/CubeMap2.dds", &pCubetex_);
	D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/Roughness.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
		D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pRoughtex_);

	int RandNum = rand() % 60;
	//scale_ = D3DXVECTOR3(0.5f,0.5f,0.5f);
	position_ = D3DXVECTOR3(-RandNum, 0, -RandNum);
	//rotate_.x += 90.0f;

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.1f, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Obstacle::Update()
{
	if (MoveFrag == FALSE)
	{
		if (position_.x == 14)
		{
			Move = TRUE;
		}
		if (position_.x == -26) {
			Move = FALSE;
		}
		
		if (Move == FALSE)
		{
			//右に行き続ける処理
			position_.x += 0.25f;
		}
		if(Move == TRUE)
		{
			//左に行き続ける処理
			position_.x -= 0.25f;
		}
	}
	FollowGround();
}

//描画
void Obstacle::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	pEffect_->SetMatrix("WVP", &matWVP);

	//pEffect_->SetMatrix("W", &worldMatrix_);

	//	D3DXMATRIX mat = worldMatrix_;
	//	mat._41 = 0;
		//	mat._42 = 0;
		//	mat._43 = 0;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);

	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

	pEffect_->SetMatrix("RS", &mat);

	//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);

	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 2, -5));
	pEffect_->SetMatrix("W", &worldMatrix_);

	//pEffect_->SetTexture("TEXTURE_TOON", pToontex_);

	pEffect_->SetTexture("ROUGH_MAP", pRoughtex_);

	pEffect_->SetTexture("TEX_CUBE", pCubetex_);

	pEffect_->Begin(NULL, 0);
	//pEffect_->BeginPass(0);
	pEffect_->BeginPass(1);

	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);

	Model::Draw(_hModel);

	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pEffect_->EndPass();

	//普通に表示
	pEffect_->BeginPass(0);
	Model::Draw(_hModel);
	pEffect_->EndPass();

	//シェーダーを使った描画終わり。
	//これ以降はDirectXデフォルトの描画に戻る

	pEffect_->End();

}

//開放
void Obstacle::Release()
{
}

void Obstacle::OnCollision(IGameObject * pTarget)
{
	//MoveFrag = TRUE;
	//プレイヤーに当たった
	if (pTarget->GetObjectName() == "Player")
	{
		//delete new SphereCollider;
		KillMe();
	}
}

//川に沿わせる
void Obstacle::FollowGround()
{
	if (hStageModel_ == -1)
	{
		//モデル番号を調べる
		hStageModel_ = ((Stage*)FindObject("Stage"))->GetModelHandle();
	}

	//もう川のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data;
		data.start = position_;					//障害物の原点から
		data.start.y = 0;						//高さ0（川は一番高いところでもY<0になっている）
		data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//川に対してレイを撃つ
		Model::RayCast(hStageModel_, &data);

		//レイが地面に当たったら
		if (data.hit)
		{
			//障害物の高さを川にあわせる
			//(Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//そこの標高は『-data.distメートル』ということになる)
			position_.y = -data.dist;
		}
	}

	if (hRiverModel_ == -1)
	{
		//モデル番号を調べる
		hRiverModel_ = ((River*)FindObject("River"))->GetModelHandle();
	}

	//もう川のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data;
		data.start = position_;					//障害物の原点から
		data.start.y = 0;						//高さ0（川は一番高いところでもY<0になっている）
		data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//川に対してレイを撃つ
		Model::RayCast(hRiverModel_, &data);

		//レイが地面に当たったら
		if (data.hit)
		{
			//障害物の高さを川にあわせる
			//(Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//そこの標高は『-data.distメートル』ということになる)
			position_.y = -data.dist;
		}
	}

	
}