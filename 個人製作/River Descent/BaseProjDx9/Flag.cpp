#include "Flag.h"
#include "Engine/ResouceManager/Model.h"
#include "River.h"

//コンストラクタ
Flag::Flag(IGameObject * parent)
	:IGameObject(parent, "Flag"), _hModel(-1), pEffect_(nullptr), pToontex_(nullptr), hRiverModel_(-1)
{
}

//デストラクタ
Flag::~Flag()
{
	SAFE_RELEASE(pToontex_);
	SAFE_RELEASE(pEffect_);
}

//初期化
void Flag::Initialize()
{
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"WaterShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	_hModel = Model::Load("data/flag.fbx", pEffect_);
	assert(_hModel >= 0);

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 0.7f);
	AddCollider(collision);

	//D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/water_diff.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
		//D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToontex_);

	rotate_.y -= 90.0f;
	position_.z += 60.0f;
	position_.x -= 5.0f;
}

//更新
void Flag::Update()
{
	FollowRiver(); 
}

//描画
void Flag::Draw()
{
	Model::SetMatrix(_hModel, worldMatrix_);

	//ビュー行列
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	pEffect_->SetMatrix("WVP", &matWVP);

	//pEffect_->SetMatrix("W", &worldMatrix_);

	//	D3DXMATRIX mat = worldMatrix_;
	//	mat._41 = 0;
		//	mat._42 = 0;
		//	mat._43 = 0;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));



	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);



	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

	pEffect_->SetMatrix("RS", &mat);

	//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);

	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 2, -5));

	//ワールド行列
	pEffect_->SetMatrix("W", &worldMatrix_);

	pEffect_->SetTexture("TEXTURE_TOON", pToontex_);

	static float scroll = 0.0f;
	scroll += 0.0004f;
	pEffect_->SetFloat("SCROLL", scroll);

	pEffect_->Begin(NULL, 0);


	pEffect_->BeginPass(0);
	Model::Draw(_hModel);
	pEffect_->EndPass();


	//シェーダーを使った描画終わり。
	//これ以降はDirectXデフォルトの描画に戻る
	pEffect_->End();

}

//開放
void Flag::Release()
{
}

void Flag::FollowRiver()
{
	if (hRiverModel_ == -1)
	{
		//モデル番号を調べる
		hRiverModel_ = ((River*)FindObject("River"))->GetModelHandle();
	}

	//もう川のモデル番号を知っている
	else
	{
		//レイを撃つ準備
		RayCastData data;
		data.start = position_;					//旗の原点から
		data.start.y = 0;						//高さ0（川は一番高いところでもY<0になっている）
		data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

		//川に対してレイを撃つ
		Model::RayCast(hRiverModel_, &data);

		//レイが地面に当たったら
		if (data.hit)
		{
			//旗の高さを川にあわせる
			//(Y=0の高さからレイ撃って、data.distメートル先に地面があったということは
			//そこの標高は『-data.distメートル』ということになる)
			position_.y = -data.dist;
		}
	}
}