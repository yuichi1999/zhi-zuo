#include "PlayScene.h"
#include "Player.h"
#include "Plane.h"
#include "Screen.h"
#include "Obstacle.h"
#include "fall_water.h"
#include "beyond_water.h"
#include "Stage.h"
#include "River.h"
#include "Flag.h"


//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene") 
	,ENEMY_NUM(15),TimeFlag(FALSE)
{
}

//初期化
void PlayScene::Initialize()
{
	pPlayer = CreateGameObject<Player>(this);		//プレイヤー表示
	CreateGameObject<River>(this);		//川表示
	CreateGameObject<Stage>(this);		//ステージ表示
	CreateGameObject<Flag>(this);
	
	//ENEMY_NUMの数、障害物を表示
	for (int a = 0; a < ENEMY_NUM ; a++)
	{
		CreateGameObject<Obstacle>(this);	//障害物表示
	}

	//CreateGameObject<Plane>(this);	
	//CreateGameObject<Screen>(this);
	//CreateGameObject<fall_water>(this);	//障害物表示
	//CreateGameObject<beyond_water>(this);	//障害物表
	
	
	
}

//更新
void PlayScene::Update()
{
	
}

//描画
void PlayScene::Draw()
{
	

}

//開放
void PlayScene::Release()
{
	
}

