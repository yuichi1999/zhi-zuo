//#pragma once
//#include "Engine/GameObject/GameObject.h"
//
////◆◆◆を管理するクラス
//class Screen : public IGameObject
//{
//	struct Vertex
//	{
//		D3DXVECTOR3 pos;
//		DWORD       color;
//		D3DXVECTOR2 uv;
//	};
//
//	//頂点バッファ
//	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
//
//	//インデックスバッファ
//	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;
//
//	//レンダーターゲット類
//	LPDIRECT3DTEXTURE9 renderTarget_;
//	LPDIRECT3DSURFACE9 depthBuffer_;
//	LPDIRECT3DSURFACE9 surface_;
//
//	//本来のターゲット情報
//	LPDIRECT3DSURFACE9 originDB_;
//	LPDIRECT3DSURFACE9 originSF_;
//
//	LPD3DXEFFECT	pEffect_;
//
//public:
//	//コンストラクタ
//	Screen(IGameObject* parent);
//
//	//デストラクタ
//	~Screen();
//
//	//初期化
//	void Initialize() override;
//
//	//更新
//	void Update() override;
//
//	//描画
//	void Draw() override;
//
//	//開放
//	void Release() override;
//};