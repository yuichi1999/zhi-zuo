//グローバル変数(アプリ側から渡される情報)
float4x4 WVP;	//ワールド、ビュー、プロジェクションの行列を合成
float4x4 W;		//ワールド行列(法線回す用)
float4 LIGHT_DIR;
float4 DIFFUSE_COLOR;

//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos		:SV_POSITION;	//位置
	float4 color	:COLOR;			//色
};


//引数:ローカル座標 3種類ある座標のうち1座標を渡す   ,3次元座標を受け取って2次元座標を返す
//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL)
//    　　セマンティクス
{
	//仮のライト
	float4 lightDir = -LIGHT_DIR;
	lightDir = normalize(lightDir);

	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);

	normal = mul(normal, W);	//オブジェクトが変形したら法線も変形
	outData.color = dot(normal, lightDir);
	outData.color.a = 1;

	outData.color *= DIFFUSE_COLOR;

	//outData.color += float4(0.2, 0.2, 0.2, 0);

	return outData;

	//WorldViewProjection
}

//ピクセルシェーダー
//2次元座標を受け取って1次元座標を返す
float4 PS(VS_OUT inData) : COLOR
{
	return inData.color;
}

//テクニック
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}