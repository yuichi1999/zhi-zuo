//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;	//ワールド、ビュー、プロジェクション行列を合成したもの
float4x4 RS;	//回転行列と拡大の逆行列
float4x4 W;		//ワールド行列
float4	 LIGHT_DIR;
float4	 DIFFUSE_COLOR;
float4	 AMBIENT_COLOR;
float4	 SPECULER_COLOR;
float	 SPECULER_POWER;
float4	 CAMERA_POS;	//視点（カメラの位置）
bool	 IS_TEXTURE;
float	 SCROLL;
texture	 TEXTURE;
texture	 NORMAL_MAP;


//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler normalSampler = sampler_state
{
	Texture = <NORMAL_MAP>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float3 eye	  : TEXCOORD1;		//視線（面の向きに修正）
	float2 uv	  : TEXCOORD0;		//UV座標
	float3 light  : TEXCOORD2;		//ライト（面の向きに修正）
};



//【頂点シェーダー】
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0, float3 tangent : TANGENT)
{
	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);			//位置（今まで通り）

	float3 binormal = cross(tangent, normal);


	normal = mul(normal, RS);				//オブジェクトが変形すれば法線も変形
	tangent = mul(tangent, RS);
	binormal = mul(binormal, RS);

	normal = normalize(normal);			//法線を正規化
	tangent = normalize(tangent);
	binormal = normalize(binormal);


	float4 worldPos = mul(pos, W);
	float3 eye = normalize(CAMERA_POS - worldPos);
	outData.eye.x = dot(eye, tangent);
	outData.eye.y = dot(eye, binormal);
	outData.eye.z = dot(eye, normal);

	outData.light.x = dot(LIGHT_DIR, tangent);
	outData.light.y = dot(LIGHT_DIR, binormal);
	outData.light.z = dot(LIGHT_DIR, normal);

	outData.uv = uv;

	//まとめて出力
	return outData;
}




//【ピクセルシェーダー】
float4 PS(VS_OUT inData) : COLOR
{
	float3 normal = (tex2D(normalSampler, inData.uv + SCROLL) * 2 - 1)
				   +(tex2D(normalSampler, inData.uv - SCROLL) * 2 - 1);
normal = normalize(normal);


	inData.eye = normalize(inData.eye);



	float3 lightDir = inData.light;
	lightDir = normalize(lightDir);	//向きだけが必要なので正規化

	//拡散反射光
	float4 diffuse = saturate(dot(normal, -lightDir));
	diffuse.a = 1;

	if (IS_TEXTURE == false)
	{
		diffuse *= DIFFUSE_COLOR;
	}
	else
	{
		diffuse *= tex2D(texSampler, inData.uv);
	}

	diffuse.a = 0.5f;

	//環境光
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	float3 R = reflect(lightDir, normal);	//正反射ベクトル
	float4 speculer = pow(saturate(dot(R, inData.eye)),15) * 1;

	//頂点シェーダーで色を求めてるので、そのまま出力
	float4 color =  ambient + diffuse + speculer;
	color.a = (color.r + color.g + color.b) / 3 + 0.6;
	return color;
}



//【テクニック】
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}