#include "River.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
River::River(IGameObject * parent)
	:IGameObject(parent, "River"), hModel_(-1), pEffect_(nullptr), pToontex_(nullptr)
{
}

//デストラクタ
River::~River()
{
	SAFE_RELEASE(pToontex_);
	SAFE_RELEASE(pEffect_);
}

//初期化
void River::Initialize()
{
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"WaterShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}


	//モデルデータのロード
	/*hModel_ = Model::Load("data/donut2.fbx", pEffect_);
	assert(hModel_ >= 0);*/

	hModel_ = Model::Load("data/stage3.fbx", pEffect_);
	assert(hModel_ >= 0);



	D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data/water_diff.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
		D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToontex_);

	//pEffect_->SetTexture("TEXTURE_TOON", pToontex_);
	//assert(pEffect_ >= 0);

	/*scale_ = D3DXVECTOR3(1.0f, 1.0f, 3.0f);
	position_.y -= 1.1f;
	position_.z -= 29.5f;*/
	position_.x -= 5.3f;
	rotate_.y += 90.0f;
	//position_.z += 30.0f;
}

//更新
void River::Update()
{
}

//描画
void River::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_ );

	//ビュー行列
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	pEffect_->SetMatrix("WVP", &matWVP);

	//pEffect_->SetMatrix("W", &worldMatrix_);

	//	D3DXMATRIX mat = worldMatrix_;
	//	mat._41 = 0;
		//	mat._42 = 0;
		//	mat._43 = 0;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);



	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY ;

	pEffect_->SetMatrix("RS", &mat);

	//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);

	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 2, -5));

	//ワールド行列
	pEffect_->SetMatrix("W", &worldMatrix_);

	pEffect_->SetTexture("TEXTURE_TOON", pToontex_);

	static float scroll = 0.0f;
	scroll += 0.0004f;
	pEffect_->SetFloat("SCROLL", scroll);

	pEffect_->Begin(NULL, 0);


	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();


	//シェーダーを使った描画終わり。
	//これ以降はDirectXデフォルトの描画に戻る
	pEffect_->End();

}

//開放
void River::Release()
{
}