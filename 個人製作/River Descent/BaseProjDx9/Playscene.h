#pragma once

#include "Engine/global.h"


class Player;
//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int ENEMY_NUM;

	Player* pPlayer;

	

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	bool TimeFlag;

	Player* GetPlayer()
	{
		return pPlayer;
	}

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	
};
