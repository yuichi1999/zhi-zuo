#pragma once
#include "Engine/GameObject/GameObject.h"

//旗を管理するクラス
class Flag : public IGameObject
{
	int _hModel;    //モデル番号

	int hRiverModel_;

	LPD3DXEFFECT	pEffect_;

	LPDIRECT3DTEXTURE9	pToontex_;

	void FollowRiver();	//川に沿わせる
public:
	//コンストラクタ
	Flag(IGameObject* parent);

	//デストラクタ
	~Flag();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};