#pragma once
#include "Engine/GameObject/GameObject.h"

class view;
class Playscene;

//プレイヤーを管理するクラス
class Player : public IGameObject
{
	int hModel_;    //モデル番号
	int hRiverModel_; //川のモデル番号
	int hStageModel_;

	bool MoveFlag;

	view *_pview;
	Playscene *_pPlay;

	void FollowGround();	//川に沿わせる

	LPD3DXEFFECT	pEffect_;

	//LPDIRECT3DTEXTURE9	pToontex_;

	int speedA;	//左移動の慣性用
	int speedD; //右移動の慣性用
	int speedW; //上移動の慣性用
	int speedS; //下移動の慣性用

	LPDIRECT3DTEXTURE9	pRoughtex_;

	LPDIRECT3DCUBETEXTURE9	pCubetex_;

public:
	int hp;
	//コンストラクタ
	Player(IGameObject* parent);

	view* Getview()
	{
		//ポインタ
		return _pview;
	}

	Playscene* Getplay()
	{
		//ポインタ
		return _pPlay;
	}
	
	

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Move();

	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;

};