#pragma once
#include "Engine/GameObject/GameObject.h"

//障害物を管理するクラス
class Obstacle : public IGameObject
{
	int _hModel;    //モデル番号

	int hRiverModel_; //川のモデル番号
	int hStageModel_;
	
	//川に沿わせる
	void FollowGround();

	bool Move;
	bool MoveFrag;

	LPD3DXEFFECT	pEffect_;

	//LPDIRECT3DTEXTURE9	pToontex_;


	LPDIRECT3DTEXTURE9	pRoughtex_;

	LPDIRECT3DCUBETEXTURE9	pCubetex_;
public:
	//コンストラクタ
	Obstacle(IGameObject* parent);

	//デストラクタ
	~Obstacle();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;
};