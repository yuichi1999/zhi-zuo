#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/DirectX/Text.h"

#define MIN_COUNT 9

class Player;

//視点を管理するクラス
class view : public IGameObject
{
	int _hModel;

	int _hImage[4];	//hp画像

	int frame;

	Player *_pPlayer;

	bool TimeCnt;

	Text* _pText;    //テキスト
	Text* _pText2;    //テキスト
	Text* _pText3;    //テキスト

	char *sec_[11];

	char *min_[11];

	//フレームをカウントする変数
	int frame_;

	//１のくらいのカウント用
	int cnt1_;

	//１０のくらいのカウント用
	int cnt10_;

	//分カウント配列の添字用
	int minCnt_;

	//void hit();
	int Hp;
public:
	//int hp;
	//コンストラクタ
	view(IGameObject* parent);

	bool TimeFlag;
	Player* GetPlayer()
	{
		//ポインタ
		return _pPlayer;
	}
	int preHp;
	//デストラクタ
	~view();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//時間をセット
	void InitTime();

	//タイムカウント
	void TimeCount();

	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;
};