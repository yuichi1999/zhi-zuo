texture	 TEXTURE;

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Mirror;
	AddressV = Mirror;
};

struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float2 uv	  : TEXCOORD0;		//UV座標
};

VS_OUT VS(float4 pos : POSITION, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	outData.pos = pos;
	outData.uv = uv;

	return outData;
}

float4 PS_Hard(VS_OUT inData) : COLOR
{
	return tex2D(texSampler,inData.uv);
}

float4 PS_Soft(VS_OUT inData) : COLOR
{
	int power = 15;
	int count = 0;

	float4 color = float4(0,0,0,0);

	for (int x = -power; x <= power; x++)
	{
		for (int y = -power; y <= power; y++)
		{
			float4 c = tex2D(texSampler,
				float2(inData.uv.x + (1.0 / 1280) * x,
					inData.uv.y + (1.0 / 720) * y));
			if (c.r + c.g + c.b < 1.7)
				 {
				c = float4(0, 0, 0, 1);
				}
			else
				 {
				c = float4(1, 1, 1, 1);
				}
			
			color += c;
			count++;
		}
	}

	color /= count;




	return color;
	//return  tex2D(texSampler, inData.uv);
}

technique
{
	//普通に表示
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Hard();
	}

	//ぼかし表示
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS_Soft();
	}
}