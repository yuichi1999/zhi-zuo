#include "GameOverScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
GameOverScene::GameOverScene(IGameObject * parent)
	: IGameObject(parent, "GameOverScene"), hPict_(-1)
{
}

//初期化
void GameOverScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("data/pict/GameOver.png");
	assert(hPict_ >= 0);
}

//更新
void GameOverScene::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void GameOverScene::Draw()
{
	Image::SetMatrix(hPict_, worldMatrix_);
	Image::Draw(hPict_);
}

//開放
void GameOverScene::Release()
{
}