//#include "Screen.h"
//#include "Engine/DirectX/Direct3D.h"
//
////コンストラクタ
//Screen::Screen(IGameObject * parent)
//	:IGameObject(parent, "Screen")
//{
//}
//
////デストラクタ
//Screen::~Screen()
//{
//}
//
////初期化
//void Screen::Initialize()
//{
//	//頂点情報(位置＆UV)
//	Vertex vertexList[] = {
//		D3DXVECTOR3(-1,  1, 0), D3DXCOLOR(255, 255, 255, 255), D3DXVECTOR2(0, 0),
//		D3DXVECTOR3( 1,  1, 0), D3DXCOLOR(255, 255, 255, 255), D3DXVECTOR2(1, 0),
//		D3DXVECTOR3( 1, -1, 0), D3DXCOLOR(255, 255, 255, 255), D3DXVECTOR2(1, 1),
//		D3DXVECTOR3(-1, -1, 0), D3DXCOLOR(255, 255, 255, 255), D3DXVECTOR2(0, 1),
//	};
//
//	Direct3D::pDevice_->CreateVertexBuffer(sizeof(vertexList), 0,
//		D3DFVF_XYZ | D3DFVF_DIFFUSE| D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
//
//
//	//頂点バッファにデータを入れる
//	Vertex *vCopy;
//	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
//	memcpy(vCopy, vertexList, sizeof(vertexList));
//	pVertexBuffer_->Unlock();
//
//
//	//インデックス情報
//	int indexList[] = { 0, 2, 3, 0, 1, 2 };
//
//	//インデックスバッファにデータを入れる
//	Direct3D::pDevice_->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
//		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
//	assert(pIndexBuffer_ != nullptr);
//	DWORD *iCopy;
//	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
//	memcpy(iCopy, indexList, sizeof(indexList));
//	pIndexBuffer_->Unlock();
//
//	Direct3D::pDevice_->CreateTexture(1280, 720, 1,
//		D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8,
//		D3DPOOL_DEFAULT, &renderTarget_, nullptr);
//
//	Direct3D::pDevice_->CreateDepthStencilSurface(
//		1280, 720, D3DFMT_D16, D3DMULTISAMPLE_NONE,
//		0, TRUE, &depthBuffer_, nullptr);
//
//	renderTarget_->GetSurfaceLevel(0, &surface_);
//
//	//元の情報を覚えておく
//	Direct3D::pDevice_->GetRenderTarget(0, &originSF_);
//	Direct3D::pDevice_->GetDepthStencilSurface(&originDB_);
//
//	LPD3DXBUFFER err = 0;
//	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
//		"GlareShader.hlsl", NULL, NULL,
//		D3DXSHADER_DEBUG, NULL, &pEffect_,
//		&err)))
//	{
//	MessageBox(NULL,
//		(char*)err->GetBufferPointer(),
//		"シェーダーエラー", MB_OK);
//	}
//
//}
//
////更新
//void Screen::Update()
//{
//	//D3DXSaveTextureToFile("Test.png",
//	//	D3DXIFF_PNG, renderTarget_, nullptr);
//
//	Direct3D::pDevice_->SetRenderTarget(0, surface_);
//	Direct3D::pDevice_->SetDepthStencilSurface(depthBuffer_);
//}
//
////描画
//void Screen::Draw()
//{
//	Direct3D::EndDraw();
//
//	Direct3D::pDevice_->SetRenderTarget(0, originSF_);
//	Direct3D::pDevice_->SetDepthStencilSurface(originDB_);
//
//	Direct3D::BeginDraw();
//
//	pEffect_->Begin(NULL, 0);
//
//	//1パス目(普通に表示)
//	pEffect_->BeginPass(0);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);
//	Direct3D::pDevice_->SetTexture(0, renderTarget_);
//	Direct3D::pDevice_->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
//	Direct3D::pDevice_->SetIndices(pIndexBuffer_);
//	Direct3D::pDevice_->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1);
//	Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);
//
//	pEffect_->EndPass();
//
//	//2パス目(ぼかして表示)
//	pEffect_->BeginPass(1);
//
//	Direct3D::pDevice_->SetRenderState(
//		D3DRS_SRCBLEND, D3DBLEND_ONE);
//	
//		Direct3D::pDevice_->SetRenderState(
//		D3DRS_DESTBLEND, D3DBLEND_ONE);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);
//	Direct3D::pDevice_->SetTexture(0, renderTarget_);
//	Direct3D::pDevice_->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
//	Direct3D::pDevice_->SetIndices(pIndexBuffer_);
//	Direct3D::pDevice_->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1);
//	Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
//	Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
//
//	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);
//
//	Direct3D::pDevice_->SetRenderState(
//		D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
//	
//		Direct3D::pDevice_->SetRenderState(
//			D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
//
//	pEffect_->EndPass();
//	pEffect_->End();
//}
//
////開放
//void Screen::Release()
//{
//}