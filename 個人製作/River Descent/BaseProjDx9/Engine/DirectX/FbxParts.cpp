#include "FbxParts.h"
#include "Fbx.h"

//コンストラクタ
FbxParts::FbxParts(LPD3DXEFFECT	pEffect) :
	_vertexCount(0),
	_polygonCount(0),
	_indexCount(0),
	_materialCount(0),
	_pPolygonCountOfMaterial(nullptr),
	_pMaterial(nullptr),
	_pTexture(nullptr),
	_pVertexList(nullptr),
	_vertexBuffer(nullptr),
	_pIndexBuffer(nullptr),
	_pSkinInfo(nullptr),
	_ppCluster(nullptr),
	_pBoneArray(nullptr),
	_pWeightArray(nullptr),
	pEffect_(pEffect),
	verDec_(nullptr)
{

}

//デストラクタ
FbxParts::~FbxParts()
{
	SAFE_RELEASE(verDec_);
	SAFE_DELETE_ARRAY(_pBoneArray);
	SAFE_DELETE_ARRAY(_ppCluster);

	if (_pWeightArray != nullptr)
	{
		for (DWORD i = 0; i < _vertexCount; i++)
		{
			SAFE_DELETE_ARRAY(_pWeightArray[i].pBoneIndex);
			SAFE_DELETE_ARRAY(_pWeightArray[i].pBoneWeight);
		}
		SAFE_DELETE_ARRAY(_pWeightArray);
	}

	for (int i = 0; i < _materialCount; i++)
	{
		SAFE_RELEASE(_pIndexBuffer[i]);
	}

	SAFE_DELETE_ARRAY(_pIndexBuffer);
	SAFE_DELETE_ARRAY(_pTexture);
	SAFE_DELETE_ARRAY(_pMaterial);

	SAFE_DELETE_ARRAY(_pPolygonCountOfMaterial);

	SAFE_RELEASE(_vertexBuffer);
	SAFE_DELETE_ARRAY(_pVertexList);

	for (int i = 0; i < _childParts.size(); i++)
	{
		SAFE_DELETE(_childParts[i]);
	}

	_childParts.clear();
}

//マテリアル（色や質感、テクスチャ）の情報をロード
void FbxParts::InitMaterial(FbxNode *pNode, Fbx* pFbx)
{
	//ノード情報のアドレスを入れておく
	_pNode = pNode;

	//各データを入れる準備
	_materialCount = pNode->GetMaterialCount();				//マテリアルは何個使ってる？
	_pMaterial = new D3DMATERIAL9[_materialCount];			//マテリアルを入れる配列作成
	_pTexture = new LPDIRECT3DTEXTURE9[_materialCount];		//テクスチャを入れる配列作成
	_pNormalMap = new LPDIRECT3DTEXTURE9[_materialCount];		//テクスチャを入れる配列作成


																			//マテリアル情報を1個ずつ確認していく
	for (int i = 0; i < _materialCount; i++)
	{
		//マテリアル
		FbxSurfacePhong* Surface = (FbxSurfacePhong*)pNode->GetMaterial(i);
		FbxDouble3 diffuse = Surface->Diffuse;
		FbxDouble3 ambient = Surface->Ambient;

		ZeroMemory(&_pMaterial[i], sizeof(D3DMATERIAL9));

		//ポリゴンの色
		_pMaterial[i].Diffuse.r = (float)diffuse[0];
		_pMaterial[i].Diffuse.g = (float)diffuse[1];
		_pMaterial[i].Diffuse.b = (float)diffuse[2];
		_pMaterial[i].Diffuse.a = 1.0f;

		//環境光
		_pMaterial[i].Ambient.r = (float)ambient[0];
		_pMaterial[i].Ambient.g = (float)ambient[1];
		_pMaterial[i].Ambient.b = (float)ambient[2];
		_pMaterial[i].Ambient.a = 1.0f;

		if (Surface->GetClassId().Is(FbxSurfacePhong::ClassId))
		{

			FbxDouble3 specular = Surface->Specular;
			//鏡面反射光
			_pMaterial[i].Specular.r = (float)specular[0];
			_pMaterial[i].Specular.g = (float)specular[1];
			_pMaterial[i].Specular.b = (float)specular[2];
			_pMaterial[i].Specular.a = 1.0f;
			_pMaterial[i].Power = (float)Surface->Shininess;
		}

		//普通のテクスチャダウンロード
		{
			//テクスチャ情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャ使ってない場合
			if (textureFile == nullptr)
			{
				_pTexture[i] = nullptr;
			}

			//テクスチャ使ってる場合
			else
			{
				_pTexture[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}

		}


		//ノーマルマップのロード
		{
			//テクスチャ情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sBump);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//ノーマルマップ使ってない場合
			if (textureFile == nullptr)
			{
				_pNormalMap[i] = nullptr;
			}

			//ノーマルマップ使ってる場合
			else
			{
				_pNormalMap[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}

		}

	}
}

//頂点バッファの作成
void FbxParts::InitVertexBuffer(FbxMesh * pMesh)
{
	//とりあえず専用のデータ型に入れる
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//各情報を調べる
	_vertexCount = pMesh->GetControlPointsCount();	//頂点数
	_polygonCount = pMesh->GetPolygonCount();		//ポリゴン数
	_indexCount = pMesh->GetPolygonVertexCount();	//インデックス数

															//頂点データを入れる配列作成
	_pVertexList = new FbxParts::Vertex[_vertexCount];


	/////////////////////////頂点の位置/////////////////////////////////////

	//1個ずつ頂点の位置を入れていく
	for (int i = 0; _vertexCount > i; i++)
	{
		_pVertexList[i].pos.x = (float)pVertexPos[i][0];
		_pVertexList[i].pos.y = (float)pVertexPos[i][1];
		_pVertexList[i].pos.z = (float)pVertexPos[i][2];
	}


	//ポリゴン1枚ずつ調べていく
	FbxLayerElementUV* leUV = pMesh->GetLayer(0)->GetUVs();
	for (int poly = 0; poly < _polygonCount; poly++)
	{
		//データの先頭をゲットして
		int startIndex = pMesh->GetPolygonVertexIndex(poly);

		//3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//int index = pMesh->GetPolygonVertices()[startIndex + vertex];
			int index = pMesh->GetPolygonVertex(poly, vertex);

			/////////////////////////頂点の法線/////////////////////////////////////
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(poly, vertex, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			_pVertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);
			D3DXVec3Normalize(&_pVertexList[index].normal, &_pVertexList[index].normal);


			/////////////////////////頂点のUV///////////////////
			// インデックスバッファからインデックスを取得する
			int lUVIndex = leUV->GetIndexArray().GetAt(poly * 3 + vertex);

			// 取得したインデックスから UV を取得する
			FbxVector2 lVec2 = leUV->GetDirectArray().GetAt(lUVIndex);


			// UV値セット
			_pVertexList[index].uv.x = lVec2.mData[0];
			_pVertexList[index].uv.y = 1.0f - lVec2.mData[1];	//Yは逆になってる

		}
	}


	for (int i = 0; i < _polygonCount; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		D3DXVECTOR3 tangent = CalcTangent(
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 0]].pos,
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 1]].pos,
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 2]].pos,
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 0]].uv,
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 1]].uv,
			&_pVertexList[pMesh->GetPolygonVertices()[startIndex + 2]].uv);


		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];
			_pVertexList[index].tangent = tangent;
		}
	}

	/////////////////////////////////////////////////
	//  頂点バッファを作成！！
	/////////////////////////////////////////////////

	D3DVERTEXELEMENT9 vertexElement[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION,	0},
	{ 0,12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,		0},
	{ 0,24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD,	0},
	{ 0,32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,	0},
	D3DDECL_END()
 };
	Direct3D::pDevice_->CreateVertexDeclaration(
		vertexElement, &verDec_);

	//まずカラのバッファ作って
	Direct3D::pDevice_->CreateVertexBuffer(sizeof(FbxParts::Vertex)*_vertexCount,0,0, D3DPOOL_MANAGED, &_vertexBuffer, 0);

	//ロックすればいじれる
	FbxParts::Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);

	//カラのバッファにデータコピー
	memcpy(vCopy, _pVertexList, sizeof(FbxParts::Vertex)*_vertexCount);

	//終わったらアンロック
	_vertexBuffer->Unlock();



	/////////////////////////////////////////////////
	//	頂点バッファ完成！！！！
	/////////////////////////////////////////////////
}



//インデックスバッファの作成
void FbxParts::InitIndexBuffer(FbxMesh * pMesh)
{
	/////////////////////////////////////////////////
	//	こっからインデックスバッファ
	/////////////////////////////////////////////////

	//インデックス数分の配列を作成
	_pIndexBuffer = new IDirect3DIndexBuffer9*[_materialCount];

	//マテリアルごとのポリゴン数を入れるための配列作成
	_pPolygonCountOfMaterial = new int[_materialCount];

	//マテリアルごとにインデックスバッファ作る
	for (int i = 0; i < _materialCount; i++)
	{
		//とりあえずインデックス番号入れる配列
		int* indexList = new int[_indexCount];

		//ポリゴンごとに
		int count = 0;
		for (int polygon = 0; polygon < _polygonCount; polygon++)
		{
			//そのポリゴンのマテリアルの番号をゲット
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			//今処理中のマテリアルだった
			if (materialID == i)
			{
				//3つ分
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//マテリアルごとのポリゴン数　＝　頂点数÷３
		_pPolygonCountOfMaterial[i] = count / 3;

		//インデックスバッファ作成
		Direct3D::pDevice_->CreateIndexBuffer(sizeof(int)* _indexCount, 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &_pIndexBuffer[i], 0);
		DWORD *iCopy;
		_pIndexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int)* _indexCount);
		_pIndexBuffer[i]->Unlock();
		delete[] indexList;
	}
}


//アニメーションデータの準備
void FbxParts::InitAnimation(FbxMesh * pMesh)
{
	// デフォーマ情報（ボーンとモデルの関連付け）の取得
	FbxDeformer *   pDeformer = pMesh->GetDeformer(0);
	if (pDeformer == nullptr)
	{
		//ボーン情報なし
		return;
	}


	// デフォーマ情報からスキンメッシュ情報を取得
	_pSkinInfo = (FbxSkin *)pDeformer;

	// 頂点からポリゴンを逆引きするための情報を作成する
	struct  POLY_INDEX
	{
		int *   polyIndex;      // ポリゴンの番号
		int *   vertexIndex;    // 頂点の番号
		int     numRef;         // 頂点を共有するポリゴンの数
	};

	POLY_INDEX * polyTable = new POLY_INDEX[_vertexCount];
	for (DWORD i = 0; i < _vertexCount; i++)
	{
		// 三角形ポリゴンに合わせて、頂点とポリゴンの関連情報を構築する
		// 総頂点数＝ポリゴン数×３頂点
		polyTable[i].polyIndex = new int[_polygonCount * 3];
		polyTable[i].vertexIndex = new int[_polygonCount * 3];
		polyTable[i].numRef = 0;
		ZeroMemory(polyTable[i].polyIndex, sizeof(int)* _polygonCount * 3);
		ZeroMemory(polyTable[i].vertexIndex, sizeof(int)* _polygonCount * 3);

		// ポリゴン間で共有する頂点を列挙する
		for (DWORD k = 0; k < _polygonCount; k++)
		{
			for (int m = 0; m < 3; m++)
			{
				if (pMesh->GetPolygonVertex(k, m) == i)
				{
					polyTable[i].polyIndex[polyTable[i].numRef] = k;
					polyTable[i].vertexIndex[polyTable[i].numRef] = m;
					polyTable[i].numRef++;
				}
			}
		}
	}

	// ボーン情報を取得する
	_numBone = _pSkinInfo->GetClusterCount();
	_ppCluster = new FbxCluster*[_numBone];
	for (int i = 0; i < _numBone; i++)
	{
		_ppCluster[i] = _pSkinInfo->GetCluster(i);
	}

	// ボーンの数に合わせてウェイト情報を準備する
	_pWeightArray = new FbxParts::Weight[_vertexCount];
	for (DWORD i = 0; i < _vertexCount; i++)
	{
		_pWeightArray[i].posOrigin = _pVertexList[i].pos;
		_pWeightArray[i].normalOrigin = _pVertexList[i].normal;
		_pWeightArray[i].pBoneIndex = new int[_numBone];
		_pWeightArray[i].pBoneWeight = new float[_numBone];
		for (int j = 0; j < _numBone; j++)
		{
			_pWeightArray[i].pBoneIndex[j] = -1;
			_pWeightArray[i].pBoneWeight[j] = 0.0f;
		}
	}




	// それぞれのボーンに影響を受ける頂点を調べる
	// そこから逆に、頂点ベースでボーンインデックス・重みを整頓する
	for (int i = 0; i < _numBone; i++)
	{
		int numIndex = _ppCluster[i]->GetControlPointIndicesCount();   //このボーンに影響を受ける頂点数
		int * piIndex = _ppCluster[i]->GetControlPointIndices();       //ボーン/ウェイト情報の番号
		double * pdWeight = _ppCluster[i]->GetControlPointWeights();     //頂点ごとのウェイト情報

																				 //頂点側からインデックスをたどって、頂点サイドで整理する
		for (int k = 0; k < numIndex; k++)
		{
			// 頂点に関連付けられたウェイト情報がボーン５本以上の場合は、重みの大きい順に４本に絞る
			for (int m = 0; m < 4; m++)
			{
				if (m >= _numBone)
					break;

				if (pdWeight[k] > _pWeightArray[piIndex[k]].pBoneWeight[m])
				{
					for (int n = _numBone - 1; n > m; n--)
					{
						_pWeightArray[piIndex[k]].pBoneIndex[n] = _pWeightArray[piIndex[k]].pBoneIndex[n - 1];
						_pWeightArray[piIndex[k]].pBoneWeight[n] = _pWeightArray[piIndex[k]].pBoneWeight[n - 1];
					}
					_pWeightArray[piIndex[k]].pBoneIndex[m] = i;
					_pWeightArray[piIndex[k]].pBoneWeight[m] = (float)pdWeight[k];
					break;
				}
			}

		}
	}

	//ボーンを生成
	_pBoneArray = new FbxParts::Bone[_numBone];
	for (int i = 0; i < _numBone; i++)
	{
		// ボーンのデフォルト位置を取得する
		FbxAMatrix  matrix;
		_ppCluster[i]->GetTransformLinkMatrix(matrix);

		// 行列コピー（Fbx形式からDirectXへの変換）
		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				_pBoneArray[i].bindPose(x, y) = (float)matrix.Get(x, y);
			}
		}
	}

	// 一時的なメモリ領域を解放する
	for (DWORD i = 0; i < _vertexCount; i++)
	{
		SAFE_DELETE_ARRAY(polyTable[i].polyIndex);
		SAFE_DELETE_ARRAY(polyTable[i].vertexIndex);
	}
	SAFE_DELETE_ARRAY(polyTable);
}


//ボーン有りのモデルを描画
void FbxParts::DrawSkinAnime(D3DXMATRIX &matrix, FbxTime time)
{
	// ボーンごとの現在の行列を取得する
	for (int i = 0; i < _numBone; i++)
	{
		FbxAnimEvaluator * evaluator = _ppCluster[i]->GetLink()->GetScene()->GetAnimationEvaluator();
		FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(_ppCluster[i]->GetLink(), time);

		// 行列コピー（Fbx形式からDirectXへの変換）
		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				_pBoneArray[i].newPose(x, y) = (float)mCurrentOrentation.Get(x, y);
			}
		}

		// オフセット時のポーズの差分を計算する
		D3DXMatrixInverse(&_pBoneArray[i].diffPose, nullptr, &_pBoneArray[i].bindPose);
		_pBoneArray[i].diffPose *= _pBoneArray[i].newPose;
	}

	// 各ボーンに対応した頂点の変形制御
	for (DWORD i = 0; i < _vertexCount; i++)
	{
		// 各頂点ごとに、「影響するボーン×ウェイト値」を反映させた関節行列を作成する
		D3DXMATRIX  matrix;
		ZeroMemory(&matrix, sizeof(matrix));
		for (int m = 0; m < _numBone; m++)
		{
			if (_pWeightArray[i].pBoneIndex[m] < 0)
			{
				break;
			}
			matrix += _pBoneArray[_pWeightArray[i].pBoneIndex[m]].diffPose * _pWeightArray[i].pBoneWeight[m];

		}

		// 作成された関節行列を使って、頂点を変形する
		D3DXVECTOR3 Pos = _pWeightArray[i].posOrigin;
		D3DXVECTOR3 Normal = _pWeightArray[i].normalOrigin;
		D3DXVec3TransformCoord(&_pVertexList[i].pos, &Pos, &matrix);
		D3DXVec3TransformCoord(&_pVertexList[i].normal, &Normal, &matrix);

	}

	// 頂点バッファをロックして、変形させた後の頂点情報で上書きする
	FbxParts::Vertex * pv;
	if (_vertexBuffer != nullptr && SUCCEEDED(_vertexBuffer->Lock(0, 0, (void**)&pv, 0)))
	{
		memcpy(pv, _pVertexList, sizeof(FbxParts::Vertex)* _vertexCount);
		_vertexBuffer->Unlock();
	}

	Draw(matrix);
}


//ボーン無しのモデルを描画
void FbxParts::DrawMeshAnime(D3DXMATRIX &matrix, FbxTime time, FbxScene *scene)
{
	// その瞬間の自分の姿勢行列を得る
	FbxAnimEvaluator *evaluator = scene->GetAnimationEvaluator();
	FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(_pNode, time);

	// Fbx形式の行列からDirectX形式の行列へのコピー（4×4の行列）
	for (DWORD x = 0; x < 4; x++)
	{
		for (DWORD y = 0; y < 4; y++)
		{
			_localMatrix(x, y) = (float)mCurrentOrentation.Get(x, y);
		}
	}

	Draw(matrix);
}



//描画（実際に描画しているのはここ）
void FbxParts::Draw(D3DXMATRIX &matrix)
{
	//頂点バッファの設定
	Direct3D::pDevice_->SetStreamSource(0, _vertexBuffer, 0, sizeof(FbxParts::Vertex));

	//頂点ストリームを指定     
	//Direct3D::pDevice_->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	Direct3D::pDevice_->SetVertexDeclaration(verDec_);

	//ワールド行列をセット（パーツごとの行列を先にかける）
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &matrix);

	//マテリアルごとに描画
	for (int i = 0; i < _materialCount; i++)
	{
		Direct3D::pDevice_->SetIndices(_pIndexBuffer[i]);
		//Direct3D::pDevice_->SetTexture(0, _pTexture[i]);
		Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, _vertexCount, 0, _pPolygonCountOfMaterial[i]);

		if (pEffect_)
		{
			pEffect_->SetVector("DIFFUSE_COLOR",
				&D3DXVECTOR4(
					_pMaterial[i].Diffuse.r,
					_pMaterial[i].Diffuse.g,
					_pMaterial[i].Diffuse.b,
					_pMaterial[i].Diffuse.a));

			pEffect_->SetVector("AMBIENT_COLOR", &D3DXVECTOR4(
				_pMaterial[i].Ambient.r,
				_pMaterial[i].Ambient.g,
				_pMaterial[i].Ambient.b,
				_pMaterial[i].Ambient.a));

			pEffect_->SetVector("SPECULER_COLOR",
				&D3DXVECTOR4(
					_pMaterial[i].Specular.r,
					_pMaterial[i].Specular.g,
					_pMaterial[i].Specular.b,
					_pMaterial[i].Specular.a));
			
				pEffect_->SetFloat("SPECULER_POWER",
					_pMaterial[i].Power);

				pEffect_->SetBool("IS_TEXTURE", _pTexture[i] != nullptr);
				pEffect_->SetTexture("TEXTURE", _pTexture[i] );
				pEffect_->SetTexture("NORMAL_MAP", _pNormalMap[i]);

		}
		else
		{
			Direct3D::pDevice_->SetTexture(0, _pTexture[i]);
			Direct3D::pDevice_->SetMaterial(&_pMaterial[i]);
		}
	}
}


//任意のボーンの位置を取得
bool FbxParts::GetBonePosition(std::string boneName, D3DXVECTOR3* position)
{
	for (int i = 0; i < _numBone; i++)
	{
		if (boneName == _ppCluster[i]->GetLink()->GetName())
		{
			FbxAMatrix  matrix;
			_ppCluster[i]->GetTransformLinkMatrix(matrix);

			position->x = (float)matrix[3][0];
			position->y = (float)matrix[3][1];
			position->z = (float)matrix[3][2];

			return true;
		}

	}

	return false;
}


//レイキャスト（レイを飛ばして当たり判定）
void FbxParts::RayCast(RayCastData * data)
{
	data->hit = FALSE;

	//頂点バッファをロック
	Vertex *vCopy;
	_vertexBuffer->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < _materialCount; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		_pIndexBuffer[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < _pPolygonCountOfMaterial[i]; j++)
		{
			//3頂点
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

			BOOL  hit;
			float dist;

			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			if (hit && dist < data->dist)
			{
				data->hit = TRUE;
				data->dist = dist;
			}
		}

		//インデックスバッファ使用終了
		_pIndexBuffer[i]->Unlock();
	}

	//頂点バッファ使用終了
	_vertexBuffer->Unlock();
}

D3DXVECTOR3 FbxParts::CalcTangent(
	D3DXVECTOR3 *pos0, D3DXVECTOR3 *pos1, D3DXVECTOR3 *pos2,
	D3DXVECTOR2 *uv0, D3DXVECTOR2 *uv1, D3DXVECTOR2 *uv2)
{
	D3DXVECTOR3 cp0[3] = {
		D3DXVECTOR3(pos0->x, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->y, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->z, uv0->x, uv0->y)
	};

	D3DXVECTOR3 cp1[3] = {
		D3DXVECTOR3(pos1->x, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->y, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->z, uv1->x, uv1->y)
	};

	D3DXVECTOR3 cp2[3] = {
		D3DXVECTOR3(pos2->x, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->y, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->z, uv2->x, uv2->y)
	};

	float f[3];
	for (int i = 0; i < 3; i++)
	{
		D3DXVECTOR3 v1 = cp1[i] - cp0[i];
		D3DXVECTOR3 v2 = cp2[i] - cp1[i];
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &v1, &v2);

		f[i] = -cross.y / cross.x;
	}

	D3DXVECTOR3 tangent;
	memcpy(tangent, f, sizeof(float) * 3);
	D3DXVec3Normalize(&tangent, &tangent);
	return tangent;
}