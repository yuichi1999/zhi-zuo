#include "Sprite.h"

//コンストラクタ
Sprite::Sprite():
	_pSprite(nullptr),
	_pTexture(nullptr)
{

}

//デストラクタ
Sprite::~Sprite()
{
	SAFE_RELEASE(_pTexture);
	SAFE_RELEASE(_pSprite);
}


//画像ファイルの読み込み
HRESULT Sprite::Load(std::string fileName)
{
	//スプライトオブジェクト作成
	D3DXCreateSprite(Direct3D::pDevice_, &_pSprite);

	//テクスチャオブジェクトの作成
	HRESULT hr;
	hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice_, fileName.c_str(), 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &_pTexture);

	return hr;
}


//描画
void Sprite::Draw(D3DXMATRIX & matrix, RECT* rect)
{
	//変換行列をセット
	_pSprite->SetTransform(&matrix);

	//描画
	_pSprite->Begin(D3DXSPRITE_ALPHABLEND);
	_pSprite->Draw(_pTexture, rect, nullptr, nullptr, D3DCOLOR_ARGB(255, 255, 255, 255));
	_pSprite->End();
}

//テクスチャのサイズを取得
D3DXVECTOR2 Sprite::GetTextureSize()
{
	D3DXVECTOR2 size;
	D3DSURFACE_DESC d3dds;
	_pTexture->GetLevelDesc(0, &d3dds);
	size.x = d3dds.Width;
	size.y = d3dds.Height;

	return size;
}
