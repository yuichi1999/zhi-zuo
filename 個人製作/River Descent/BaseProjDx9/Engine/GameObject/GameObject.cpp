#include "gameObject.h"
#include <assert.h>
#include "../global.h"

//コンストラクタ（親も名前もなし）
IGameObject::IGameObject(void) :
	IGameObject(nullptr, "")
{
	_colliderList.clear();
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) : 
	IGameObject(parent, "")
{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string& name)
	: _Parent(parent),
	objectName_(name),
	position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)),
	scale_(D3DXVECTOR3(1, 1, 1))
{
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
	_ChildList.clear();
	_State = { 0, 1, 1, 0 };
}

//デストラクタ
IGameObject::~IGameObject()
{
	for (auto it = _colliderList.begin(); it != _colliderList.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	_colliderList.clear();
}

// 削除するかどうか
bool IGameObject::IsDead()
{
	return (_State.dead != 0);
}

// 自分を削除する
void IGameObject::KillMe()
{
	_State.dead = 1;
}

// Updateを許可
void IGameObject::Enter()
{
	_State.entered = 1;
}

// Updateを拒否
void IGameObject::Leave()
{
	_State.entered = 0;
}

// Drawを許可
void IGameObject::Visible()
{
	_State.visible = 1;
}

// Drawを拒否
void IGameObject::Invisible()
{
	_State.visible = 0;
}

// 初期化済みかどうか
bool IGameObject::IsInitialized()
{
	return (_State.initialized != 0);
}

// 初期化済みにする
void IGameObject::SetInitialized()
{
	_State.initialized = 1;
}

// Update実行していいか
bool IGameObject::IsEntered()
{
	return (_State.entered != 0);
}

// Draw実行していいか
bool IGameObject::IsVisibled()
{
	return (_State.visible != 0);
}

//子オブジェクトリストを取得
std::list<IGameObject*>* IGameObject::GetChildList()
{
	return &_ChildList;
}

//親オブジェクトを取得
IGameObject * IGameObject::GetParent(void)
{
	return _Parent;
}

//名前でオブジェクトを検索（対象は自分の子供以下）
IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいないなら終わり
	if (_ChildList.empty())
		return nullptr;

	//イテレータ
	auto it = _ChildList.begin();	//先頭
	auto end = _ChildList.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}

//オブジェクトの名前を取得
const std::string& IGameObject::GetObjectName(void) const
{
	return objectName_;
}

//子オブジェクトを追加（リストの最後へ）
void IGameObject::PushBackChild(IGameObject * obj)
{
	assert(obj != nullptr);

	obj->_Parent = this;
	_ChildList.push_back(obj);
}

//子オブジェクトを追加（リストの先頭へ）
void IGameObject::PushFrontChild(IGameObject * obj)
{
	assert(obj != nullptr);

	obj->_Parent = this;
	_ChildList.push_front(obj);
}

//子オブジェクトを全て削除
void IGameObject::KillAllChildren(void)
{
	//子供がいないなら終わり
	if (_ChildList.empty())
		return;

	//イテレータ
	auto it = _ChildList.begin();	//先頭
	auto end = _ChildList.end();	//末尾

	//子オブジェクトを1個ずつ削除
	while (it != end)
	{
		KillObjectSub(*it);
		delete *it;
		it = _ChildList.erase(it);
	}

	//リストをクリア
	_ChildList.clear();
}

//オブジェクト削除（再帰）
void IGameObject::KillObjectSub(IGameObject * obj)
{
	if (!_ChildList.empty())
	{
		auto list = obj->GetChildList();
		auto it = list->begin();
		auto end = list->end();
		while (it != end)
		{
			KillObjectSub(*it);
			delete *it;
			it = list->erase(it);
		}
		list->clear();
	}
	obj->Release();
}

//ワールド行列の作成
void IGameObject::Transform()
{
	//移動行列
	D3DXMATRIX trans;
	D3DXMatrixTranslation(&trans, position_.x, position_.y, position_.z);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);


	//ローカル行列
	localMatrix_ = scale * rotateZ * rotateX * rotateY * trans;

	//ワールド行列
	if (GetParent() == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * GetParent()->GetWorldMatrix();
	}
}



//コライダー（衝突判定）を追加する
void IGameObject::AddCollider(Collider* collider)
{
	collider->SetGameObject(this);
	_colliderList.push_back(collider);
}


//衝突判定
void IGameObject::Collision(IGameObject * pTarget)
{
	//自分同士の当たり判定はしない
	if (this == pTarget)
	{
		return;
	}

	//自分とpTargetのコリジョン情報を使って当たり判定
	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->_colliderList.begin(); i != this->_colliderList.end(); i++)
	{
		for (auto j = pTarget->_colliderList.begin(); j != pTarget->_colliderList.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				//当たった
				this->OnCollision(pTarget);
			}
		}
	}

	//子供がいないなら終わり
	if (pTarget->_ChildList.empty())
		return;

	//子供も当たり判定
	for(auto i = pTarget->_ChildList.begin(); i != pTarget->_ChildList.end(); i++)
	{
		Collision(*i);
	}
}


//テスト用の衝突判定枠を表示
void IGameObject::CollisionDraw()
{
	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice_->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->_colliderList.begin(); i != this->_colliderList.end(); i++)
	{
		(*i)->Draw(position_);
	}

	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, Direct3D::isLighting_);
}



//ローカル行列の取得（このオブジェクトの行列）
const D3DXMATRIX& IGameObject::GetLocalMatrix(void)
{
	return localMatrix_;
}

//ワールド行列の取得（親の影響を受けた最終的な行列）
const D3DXMATRIX& IGameObject::GetWorldMatrix(void)
{
	return worldMatrix_;
}


