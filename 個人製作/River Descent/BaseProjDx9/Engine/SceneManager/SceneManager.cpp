#include "sceneManager.h"
#include "../global.h"
#include "../ResouceManager/Model.h"
#include "../ResouceManager/Image.h"

#include "../../BootScene.h"
#include "../../PlayScene.h"
#include "../../GameOverscene.h"
#include "../../TitleScene.h"
#include "../../GameClearScene.h"



//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	: IGameObject(parent, "SceneManager")
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを準備
	_currentSceneID = SCENE_ID_TITLE;
	_nextSceneID = SCENE_ID_TITLE;
	CreateGameObject<TitleScene>(this);
}

//更新
void SceneManager::Update()
{
	//次のシーンが現在のシーンと違う　＝　シーンを切り替えなければならない
	if (_currentSceneID != _nextSceneID)
	{
		//そのシーンのオブジェクトを全削除
		KillAllChildren();

		//ロードしたデータを全削除
		Model::AllRelease();
		Image::AllRelease();

		//次のシーンを作成
		switch (_nextSceneID) 
		{
		case SCENE_ID_TITLE: CreateGameObject<TitleScene>(this); break;
		case SCENE_ID_BOOT: CreateGameObject<BootScene>(this); break;
		case SCENE_ID_PLAY: CreateGameObject<PlayScene>(this); break;
		case SCENE_ID_GAMEOVER: CreateGameObject<GameOverScene>(this); break;
		case SCENE_ID_GAMECLEAR: CreateGameObject<GameClearScene>(this); break;
		}

		_currentSceneID = _nextSceneID;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

//シーン切り替え（実際に切り替わるのはこの次のフレーム）
void SceneManager::ChangeScene(SCENE_ID next)
{
	_nextSceneID = next;
}