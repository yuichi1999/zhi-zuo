
float4x4 WVP;	
float4x4 RS;	
float4x4 W;		
float4	 LIGHT_DIR;
float4	 DIFFUSE_COLOR;
float4	 CAMERA_POS;	
texture	 TEXTURE;



sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
};



struct VS_OUT
{
	float4 pos    : SV_POSITION;	
	float4 normal : NORMAL;			
	float4 eye	  : TEXCOORD1;		
	float2 uv	  : TEXCOORD0;		
};




VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	
	VS_OUT outData;

	outData.pos = mul(pos, WVP);			

	normal = mul(normal, RS);				
	normal = normalize(normal);			
	outData.normal = normal;

	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;


	
	return outData;
}





float4 PS(VS_OUT inData) : COLOR
{
	float4 lightDir = LIGHT_DIR;	
	lightDir = normalize(lightDir);	

	
	float4 diffuse = dot(inData.normal, -lightDir);
	diffuse.a = 1;
	
	diffuse *= tex2D(texSampler, inData.uv);

	
	float4 ambient = float4(0.2, 0.2, 0.2, 0);

	
	float speSize = 40.0f;
	float spePower = 2.0f;
	float4 R = reflect(lightDir, inData.normal);	
	float4 speculer = pow(dot(R, inData.eye), speSize) * spePower;


	
	return ambient + diffuse + speculer;
}




technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}