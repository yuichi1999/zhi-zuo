//グローバル変数(アプリ側から渡される情報)
float4x4 WVP;

        //引数:ローカル座標 3種類ある座標のうち1座標を渡す   ,3次元座標を受け取って2次元座標を返す
//頂点シェーダー
float4 VS(float4 pos : POSITION) : SV_POSITION
                     //    　　セマンティクス
{
						   //WorldViewProjection
	
	float4 outPos = mul(pos, WVP);
	outPos.x += 1;
	return outPos;
}

//ピクセルシェーダー
//2次元座標を受け取って1次元座標を返す
float4 PS(float4 pos : SV_POSITION) : COLOR
{               //R  G  B  α
	float4 color = float4(1, 0, 0, 1);
	color.g = pos.x / 1280;
	/*if (pos.x > 640)
	{
		color.g = 1;
	}*/
	return color;
}

technique
{
	pass
	{   
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}