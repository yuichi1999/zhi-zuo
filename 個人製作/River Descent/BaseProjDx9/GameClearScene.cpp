#include "GameClearScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
GameClearScene::GameClearScene(IGameObject * parent)
	: IGameObject(parent, "GameClearScene"), _hPict(-1)
{
}

//初期化
void GameClearScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Pict/GameClear.png");
	assert(_hPict >= 0);
}

//更新
void GameClearScene::Update()
{
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void GameClearScene::Draw()
{
	Image::SetMatrix(_hPict, worldMatrix_);
	Image::Draw(_hPict);
}

//開放
void GameClearScene::Release()
{
}