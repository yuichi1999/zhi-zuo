#include "GameOverScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
GameOverScene::GameOverScene(IGameObject * parent)
	: IGameObject(parent, "GameOverScene"), _hPict(-1)
{
}

//初期化
void GameOverScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Pict/win.png");
	assert(_hPict >= 0);
}

//更新
void GameOverScene::Update()
{
}

//描画
void GameOverScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void GameOverScene::Release()
{
}