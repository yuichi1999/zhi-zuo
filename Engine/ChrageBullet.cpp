#include "ChargeBullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
ChargeBullet::ChargeBullet(IGameObject * parent)
	:IGameObject(parent, "ChargeBullet"), _hModel(-1),SPEED(0.5f),_move(D3DXVECTOR3(0, 0, 0)), _dy(0.0f)
{
}

//デストラクタ
ChargeBullet::~ChargeBullet()
{
}

//初期化
void ChargeBullet::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/ChargeBullet.fbx");
	assert(_hModel >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);

}

//更新
void ChargeBullet::Update()
{
	//移動
	_position += _move;
	_move.y += _dy;
	_dy -= 0.001f;

	//ある程度低い位置まで落ちたら消す
	if (_position.y < -1)
	{
		KillMe();
	}
}

//描画
void ChargeBullet::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void ChargeBullet::Release()
{
}

//発射
void ChargeBullet::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	_position = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&_move, &direction);
	_move *= SPEED;
}