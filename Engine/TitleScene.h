#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"
#include <stdio.h>
#include <string>

#define MIN_COUNT 7		


//■■シーンを管理するクラス
class TitleScene : public IGameObject
{
	Text* _pText;    //テキスト
	Text* _pText2;    //テキスト
	Text* _pText3;    //テキスト

	int _hImage[5];    //画像番号

	char *sec_[11];

	char *min_[11];

	//フレームをカウントする変数
	int frame_;

	//１のくらいのカウント用
	int cnt1_;

	//１０のくらいのカウント用
	int cnt10_;

	//分カウント配列の添字用
	int minCnt_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void InitTime();

	void TimeCount();
};
