#include "EnemyHead.h"
#include "Engine/ResouceManager/Model.h"
#include "EnemyBullet.h"
#include "Engine/DirectX/Direct3D.h"

//コンストラクタ
EnemyHead::EnemyHead(IGameObject * parent)
	:IGameObject(parent, "EnemyHead"), _hModel(-1)
{
}

//デストラクタ
EnemyHead::~EnemyHead()
{
}

//初期化
void EnemyHead::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/TankHead.fbx");
	assert(_hModel >= 0);
}

//更新
void EnemyHead::Update()
{
	//Shot();

	
}

//描画
void EnemyHead::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void EnemyHead::Release()
{
}

//弾を撃つ
void EnemyHead::Shot()
{
	IGameObject* BootScene = FindObject("BootScene");
	EnemyBullet* pEnemyBullet = CreateGameObject<EnemyBullet>(FindObject("PlayScene"));

	D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel, "ShotPoint");
	D3DXVECTOR3 cannonRoot = Model::GetBonePosition(_hModel, "CannonRoot");
	pEnemyBullet->Shot(shotPos, shotPos - cannonRoot);
}