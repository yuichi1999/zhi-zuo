#pragma once
#include "Engine/global.h"
#include "Engine/GameObject/GameObject.h"


//◆◆◆を管理するクラス
class Head : public IGameObject
{
	

	int recast_;

	int frame_;		//60フレーム(一秒)単位での処理用

	int _hImage[10];

	int charge_;

	int _hModel[2];

	int _hModel2;

	
	
public:
	int aaa;

	bool Hit;

	void HitFlag(bool CHit)
	{
		Hit = CHit;
	}

	//コンストラクタ
	Head(IGameObject* parent);

	//デストラクタ
	~Head();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void Reload();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//弾を撃つ
	void Shot();

	//溜め攻撃
	void charge();

	void OnCollision(IGameObject * pTarget);
};
