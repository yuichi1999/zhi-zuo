#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Engine/gameObject/Camera.h"
#include "Head.h"
#include <windows.h>


//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), _hModel(-1), SWING_SPEED(1.5f), HP(5),
	 _pText(nullptr), _pText2(nullptr), _pText3(nullptr), frame_(0), cnt1_(0), cnt10_(0), minCnt_(HP_COUNT),
	HitFlag(FALSE), preHp(4),aaa(0),back_(0)

{
}


//デストラクタ
Player::~Player()
{
}


//初期化
void Player::Initialize()
{
	_position = D3DXVECTOR3(6, 0, 3);

	CreateGameObject<Head>(this);

	//FindObjectで
	_pStage = (Stage*)FindObject("Stage");

	P3DInitData data;
	data.pDevice = Direct3D::_pDevice;

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);
	
	//モデルデータのロード
	_hModel = Model::Load("data/Models/TankBody.fbx");
	assert(_hModel >= 0);

	
	
	////カメラ
	//Camera* pCamera = CreateGameObject<Camera>(this);
	//pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	//pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
	


	/*if (なんか押してtら)
	{

		count++
	}
	else
	{
		count--
	}*/

	/*if (_positon.x > ~ && _positon.x < ~)
	{
		if (_positon.y > ~&& _positon.x < ~)
		{

		}

	}
*/

//テキストを作成
	_pText = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText2 = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText3 = new Text("ＭＳ ゴシック", 60);

	InitHp();

}


//更新
void Player::Update()
{
	Move();
	

	if (HitFlag == TRUE)
	{
		if (preHp == HP)
		{
			preHp = HP;	
		}
		else
		{
			HpCount();
		}
	}

	//IGameObject * pTarget;
	//if (pTarget->GetObjectName() == "Enemy")
	//{
	//	HpCount();
	//	/*HP--;
	//	if (HP == 4)
	//	{
	//		HpCount();

	//	}*/
	//}
	
	//Jump(checkX, checkZ);
	
	if (aaa = 1)
	{
		back_++;
	}
}





void Player::Move()
{
	D3DXMATRIX move;
	D3DXMatrixRotationY(&move, D3DXToRadian(_rotate.y));

	//通常移動
	D3DXVECTOR3 UV = D3DXVECTOR3(0, 0, 0.1f);
	D3DXVec3TransformCoord(&UV, &UV, &move);

	D3DXVECTOR3 LR = D3DXVECTOR3(0.1f, 0, 0);
	D3DXVec3TransformCoord(&LR, &LR, &move);

	/*if (Input::IsKey(DIK_SPACE))
	{
		_rotate.y++;
	}*/
	
	//動く前の位置設定
	D3DXVECTOR3 prePos = _position;

	//→が押されていたら
	if (Input::IsKey(DIK_RIGHT))
	{
		//右へ移動
		_position += LR;

	}

	//←が押されていたら
	if (Input::IsKey(DIK_LEFT))
	{
		//左へ移動
		_position -= LR;
	}

	//↑が押されていたら
	if (Input::IsKey(DIK_UP))
	{
		//上へ移動
		_position += UV;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_DOWN))
	{
		//下へ移動
		_position -= UV;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_LSHIFT))
	{
		//
		_rotate.y -= 1.0f;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_RSHIFT))
	{
		//下へ移動
		_rotate.y += 1.0f;
	}



	//どっちに移動したか計算する
	//差分を求める
	D3DXVECTOR3 Move = _position - prePos;

	//ベクトルの長さが伸びているかどうかで
	//動いているかどうかを判断する
	float speed = D3DXVec3Length(&Move);


	//動いているかどうかで
	//ここから向き+衝突判定
	if (speed != 0)
	{
		int checkX;
		int checkZ;


		//プレイヤーの高さが1より低かったら壁の当たり判定を出現させる
		if (_position.y <= 1)
		{

			checkX = (int)(_position.x - 0.3f);
			checkZ = (int)_position.z;
			//もしプレイヤーの位置が壁だったらプレイヤーの位置をひとつ前の位置に戻す
			//このtrueは無くてもいい
			//左の当たり判定
			if (_pStage->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				_position.x = checkX + 1.3;
			}





			checkX = (int)(_position.x + 0.3f);
			checkZ = (int)_position.z;

			//右の当たり判定
			if (_pStage->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				_position.x = checkX - 0.3;
			}

			checkX = (int)_position.x;
			checkZ = (int)(_position.z + 0.3f);

			//上の当たり判定
			if (_pStage->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				_position.z = checkZ - 0.3;
			}


			checkX = (int)_position.x;
			checkZ = (int)(_position.z - 0.3f);



			//画面下の当たり判定
			//パックマンの下向きであり重力が向いてる方向ではない
			if (_pStage->IsWall(checkX, checkZ) == true)

			{
				//一個前の位置に戻す

				_position.z = checkZ + 1.3;
			}
		}
	}
}

void Player::Jump(int &checkX, int &checkZ)
{
	int checkY;
	


	checkX = (int)_position.x;
	checkZ = (int)_position.z;
	checkY = (int)(_position.y - 0.3f);



	////ここでジャンプの処理やります
	////コントローラーのBボタンを押した
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_B) || Input::IsKeyDown(DIK_C))
	{
		if (_position.y < 2 && _position.y <= 0.1)
		{
			_position.y += 0.5f;
		}
	}

	else if (checkY <= _position.y || _pStage->IsWall(checkX, checkZ) == true)
	{
		_position.y -= 0.05f;
	}

	//ジャンプボタンが押されていない間は常に落ち続けている
	else
	{
		if (_position.y > 0.1)
		{
			_position.y -= 0.05f;
		}
	}
}


//描画
void Player::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
	
	_pText->Draw(120, 600, sec_[cnt1_]); //自動で299にする
	_pText2->Draw(80, 600, sec_[cnt10_]); //自動で299にする
	_pText3->Draw(40, 600, min_[minCnt_]); //自動で299にする

	/*Model::SetMatrix(_hModel4[aaa], _localMatrix);
	Model::Draw(_hModel4[aaa]);*/
}


//開放
void Player::Release()
{
	delete _pText;
	delete _pText2;
	delete _pText3;
}

void Player::OnCollision(IGameObject * pTarget)
{
	if (pTarget->GetObjectName() == "Enchant")
	{
		HitFlag = TRUE;

		//(_pHead->aaa) = 1;

		aaa = 1;
		
		back_++;
	}

	if (back_ == 180)
	{
		aaa = 0;
	}

}


void Player::InitHp()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9]	= "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

//HP表示
void Player::HpCount()
{

	//フレームのカウントが60でちょうど1秒
	frame_++;

	if (frame_ == 1)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初500から499にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 1;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 1;
					cnt1_ = 1;
					minCnt_++;
					
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

				//HP400,300,200,100,0で止める
				int i = 6;
				for (i = 6; i <= 10; i++)
				{
					if (minCnt_ == i && cnt10_ == 10 && cnt1_ == 10)
					{
						HitFlag = FALSE;
					}

					if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
					{
						HitFlag = FALSE;
						//minCnt_ == 9 && cnt10_ == 10 && cnt1_ == 10;
					}
				}
			}
			/*if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
			{
			cnt1_ = 0;
			cnt10_ = 0;
			minCnt_ = 7;
			}*/	
		}
	}
}