#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class view : public IGameObject
{

public:
	//コンストラクタ
	view(IGameObject* parent);

	//デストラクタ
	~view();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};