#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"
#include "Engine/gameObject/Camera.h"



//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}


//デストラクタ
Stage::~Stage()
{
}


//初期化
void Stage::Initialize()
{
	


	//モデルデータのロード
	_hModel[BL_FLOOR] = Model::Load("data/Models/Floor.fbx");
	assert(_hModel[BL_FLOOR] >= 0);

	//モデルデータのロード
	_hModel[BL_WALL] = Model::Load("data/Models/Wall.fbx");
	assert(_hModel[BL_WALL] >= 0);

	CsvReader csv;
	csv.Load("data/Map.csv");


	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			_table[x][z] = csv.GetValue(x, z);
		}
	}

}



//更新
void Stage::Update()
{
}


//描画
void Stage::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			D3DXMATRIX mat;
			D3DXMatrixTranslation(&mat, x, 0, z);

			Model::SetMatrix(_hModel[_table[x][z]], mat);
			Model::Draw(_hModel[_table[x][z]]);

		}
	}
}


//開放
void Stage::Release()
{
}

