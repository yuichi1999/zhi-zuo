#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"
#include "P3DEngine.h"
#include <stdio.h>
#include <string>
#include "Engine/DirectX/Text.h"

#define HP_COUNT 5	



#if _DEBUG
#pragma comment(lib, "P3DEngineD.lib")
#else
#pragma comment(lib, "P3DEngine.lib")
#endif

class Head;

//◆◆◆を管理するクラス
class Player : public IGameObject
{
	const float SWING_SPEED;	//旋回速度

	int _hModel;    //モデル番号
	int _hModel2;    //モデル番号
	int _hModel3;    //モデル番号
	

	Stage* _pStage;
	Head* _pHead;

	bool HitFlag;

	int aaa;

	int HP;
	int preHp;

	Text* _pText;    //テキスト
	Text* _pText2;    //テキスト
	Text* _pText3;    //テキスト

	int _hModel4[2];

	char *sec_[11];

	char *min_[11];

	//フレームをカウントする変数
	int frame_;

	//１のくらいのカウント用
	int cnt1_;

	//１０のくらいのカウント用
	int cnt10_;

	//分カウント配列の添字用
	int minCnt_;
	

	int back_;

public:

	//コンストラクタ
	Player(IGameObject* parent);

	Head* GetHead()
	{
		//ポインタ
		return _pHead;
	}


	//デストラクタ
	~Player();


	//初期化
	void Initialize() override;


	//更新
	void Update() override;

	//void NewFunction();

	void Move();

	void Jump(int &checkX, int &checkZ);


	//描画
	void Draw() override;


	//開放
	void Release() override;

	void OnCollision(IGameObject * pTarget);

	void InitHp();

	void HpCount();
};

