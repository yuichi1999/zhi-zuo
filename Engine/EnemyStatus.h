#pragma once
#include "Engine/GameObject/GameObject.h"


//敵のHPを管理するクラス
class EnemyStatus : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	EnemyStatus(IGameObject* parent);

	//デストラクタ
	~EnemyStatus();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
