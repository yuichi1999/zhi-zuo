#pragma once
#include "Engine/GameObject/GameObject.h"


//ステージを管理するクラス
class Stage : public IGameObject
{
	enum
	{
		BL_FLOOR,
		BL_WALL,
		BL_MAX
	};
	
	int _hModel[BL_MAX];

	int _table[15][15];

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;


	//更新
	void Update() override;


	//描画
	void Draw() override;


	//開放
	void Release() override;


	//その位置が壁かどうかを
	//引数x,z　調べたい位置
	//戻り値　壁ならtrue 違うならfalse
	bool IsWall(int x, int z)
	{

		//if文使わなくても条件と返したいものの結果が同じだから
		//if文の条件をreturnで返せばいい
		return _table[x][z] == BL_WALL;

	}

};

