#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"
#include "P3DEngine.h"

#if _DEBUG
#pragma comment(lib, "P3DEngineD.lib")
#else
#pragma comment(lib, "P3DEngine.lib")
#endif

//◆◆◆を管理するクラス
class Enemy : public IGameObject
{
	int _hModel;    //モデル番号

	//フレームをカウントする変数
	int frame_;

	int Hp_;
	
	Stage* _pStage;
public:
	//コンストラクタ
	Enemy(IGameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void move();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;
};
