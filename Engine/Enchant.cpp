#include "Enchant.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Enchant::Enchant(IGameObject * parent)
	:IGameObject(parent, "Enchant"), _hModel(-1)
{
}

//デストラクタ
Enchant::~Enchant()
{
}

//初期化
void Enchant::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/ChargeBullet.fbx");
	assert(_hModel >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Enchant::Update()
{
}

//描画
void Enchant::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enchant::Release()
{
}

void Enchant::OnCollision(IGameObject * pTarget)
{
	if (pTarget->GetObjectName() == "Player")
	{
		KillMe();
	}
}
