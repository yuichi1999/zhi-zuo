#include "Head.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Model.h"
#include "Bullet.h"
#include "Engine/DirectX/Direct3D.h"
#include "Engine/ResouceManager/Image.h"
#include "ChargeBullet.h"
#include "Player.h"
#include "BootScene.h"


//コンストラクタ
Head::Head(IGameObject * parent)
	:IGameObject(parent, "Head"),frame_(0),
	recast_(5), charge_(0),aaa(0),_hModel2(-1)
{
}

//デストラクタ
Head::~Head()
{
}

//初期化
void Head::Initialize()
{
	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));

	std::string fileName[10] =
	{
		"Data/pict/tama0.png",	//0
		"Data/pict/tama1.png",	//1
		"Data/pict/tama2.png",	//2
		"Data/pict/tama3.png",	//3
		"Data/pict/tama4.png",	//4
		"Data/pict/tama5.png",	//5
		"Data/pict/Charge0.png",//6
		"Data/pict/Charge1.png",//7
		"Data/pict/Charge2.png",//8
		"Data/pict/Charge3.png",//9
	};

	//登録した画像を読み込み
	for (int i = 0; i <= 9; i++)
	{
		_hImage[i] = Image::Load(fileName[i]);
	}

	
	//モデルデータのロード
	_hModel2 = Model::Load("data/Models/TankHead.fbx");
	assert(_hModel >= 0);
	
}

//更新
void Head::Update()
{
	//弾を撃つ
	Shot();

	//リロード
	Reload();

	//溜め攻撃
	charge();
}

//リロード
void Head::Reload()
{
	if (recast_ == 0)
	{
		frame_++;
		if (frame_ == 180)
		{
			recast_ = 5;
		}
	}
}

//描画
void Head::Draw()
{
	Model::SetMatrix(_hModel2, _worldMatrix);
	Model::Draw(_hModel2);

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 1060, 480, 0);

	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 0.8f, 0.8f, 1.0f);

	Image::SetMatrix(_hImage[recast_], _localMatrix * a *  m);
	Image::Draw(_hImage[recast_]);

	D3DXMATRIX n;
	D3DXMatrixTranslation(&n, 930, 400, 0);
	
	if (charge_ >= 0 && charge_ < 60)
	{
		Image::SetMatrix(_hImage[6], _localMatrix * a * n);
		Image::Draw(_hImage[6]);
	}

	if (charge_ >= 60 && charge_ < 120)
	{
		Image::SetMatrix(_hImage[7], _localMatrix * a * n);
		Image::Draw(_hImage[7]);
	}

	if (charge_ >= 120 && charge_ < 180)
	{
		Image::SetMatrix(_hImage[8], _localMatrix * a * n);
		Image::Draw(_hImage[8]);
	}

	if (charge_ >= 180)
	{
		Image::SetMatrix(_hImage[9], _localMatrix * a * n);
		Image::Draw(_hImage[9]);
	}
}

//開放
void Head::Release()
{
}

//弾を撃つ
void Head::Shot()
{
	if (Input::IsKeyDown(DIK_SPACE) && recast_ > 0)
	{		
		IGameObject* BootScene = FindObject("BootScene");
		Bullet* pBullet = CreateGameObject<Bullet>(FindObject("BootScene"));

		D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel[aaa], "ShotPoint");
		D3DXVECTOR3 cannonRoot = Model::GetBonePosition(_hModel[aaa], "CannonRoot");
		pBullet->Shot(shotPos, shotPos - cannonRoot);
		frame_ = 0;

		recast_--;
	}
}

//溜め攻撃
void Head::charge()
{
	if (Input::IsKey(DIK_G))
	{
		charge_++;
	}
	/*else
	{
		charge_--;
		charge_ >= 0;
	}*/

	if (charge_ >=180 && Input::IsKeyUp(DIK_G))
	{
		IGameObject* BootScene = FindObject("BootScene");
		ChargeBullet* pChargeBullet = CreateGameObject<ChargeBullet>(FindObject("PlayScene"));

		D3DXVECTOR3 shotPos = Model::GetBonePosition(_hModel[aaa], "ShotPoint");
		D3DXVECTOR3 cannonRoot = Model::GetBonePosition(_hModel[aaa], "CannonRoot");
		pChargeBullet->Shot(shotPos, shotPos - cannonRoot);

		charge_ = 0;
	}
}

void Head::OnCollision(IGameObject * pTarget)
{
}
