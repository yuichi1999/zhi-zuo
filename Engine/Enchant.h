#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class Enchant : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	Enchant(IGameObject* parent);

	//デストラクタ
	~Enchant();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject * pTarget);
};