#include "view.h"
#include "Engine/gameObject/Camera.h"

//コンストラクタ
view::view(IGameObject * parent)
	:IGameObject(parent, "view")
{
}

//デストラクタ
view::~view()
{
}

//初期化
void view::Initialize()
{
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
}

//更新
void view::Update()
{
}

//描画
void view::Draw()
{
}

//開放
void view::Release()
{
}