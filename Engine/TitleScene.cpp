#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), _pText(nullptr), _pText2(nullptr), _pText3(nullptr), frame_(0), cnt1_(0), cnt10_(0), minCnt_(MIN_COUNT)
{
}

//初期化
void TitleScene::Initialize()
{
	//テキストを作成
	_pText = new Text("ＭＳ ゴシック", 12);
	//テキストを作成
	_pText2 = new Text("ＭＳ ゴシック", 12);
	//テキストを作成
	_pText3 = new Text("ＭＳ ゴシック", 12);

	std::string fileName[5] =
	{
		"Data/pict/one.png",
		"Data/pict/two.png",
		"Data/pict/three.png",
		"Data/pict/four.png",
		"Data/pict/five.png",
	};

	//登録した画像を読み込み
	for (int i = 0; i < 5; i++)
	{
		_hImage[i] = Image::Load(fileName[i]);
	}

	InitTime();
}

//更新
void TitleScene::Update()
{
	TimeCount();

	if(Input::IsKeyDown(DIK_RETURN))
	{ 
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_BOOT);
	}
}

//描画
void TitleScene::Draw()
{
	_pText->Draw(220, 100, sec_[cnt1_]); //自動で2:59にする
	_pText2->Draw(210, 100, sec_[cnt10_]); //自動で2:59にする
	_pText3->Draw(200, 100, min_[minCnt_]); //自動で2:59にする

	Image::SetMatrix(_hImage[3], _localMatrix);
	Image::Draw(_hImage[3]);
}

//開放
void TitleScene::Release()
{
	delete _pText;
	delete _pText2;
	delete _pText3;
}

void TitleScene::InitTime()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

void TitleScene::TimeCount()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;

	if (frame_ == 1)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初3:00から2:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 5;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 5;
					cnt1_ = 1;
					minCnt_++;
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

				
			}
			/*if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
			{
			cnt1_ = 0;
			cnt10_ = 0;
			minCnt_ = 7;
			}*/

		}
	}
}
