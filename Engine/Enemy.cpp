#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "EnemyStatus.h"
#include "EnemyHead.h"
#include <time.h>


//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), _hModel(-1),frame_(0),Hp_(5)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/TankBody.fbx");
	assert(_hModel >= 0);

	_position = D3DXVECTOR3(6, 0, 7);

	
	//FindObjectで
	_pStage = (Stage*)FindObject("Stage");

	P3DInitData data;
	data.pDevice = Direct3D::_pDevice;

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);
	
	CreateGameObject<EnemyStatus>(this);
	CreateGameObject<EnemyHead>(this);
}

//更新
void Enemy::Update()
{
	//move();

	////→が押されていたら
	//if (Input::IsKey(DIK_RIGHT))
	//{
	//	//右へ移動
	//	_rotate.y++;

	//}

	////←が押されていたら
	//if (Input::IsKey(DIK_LEFT))
	//{
	//	//左へ移動
	//	_rotate.y--;
	//}

	////↑が押されていたら
	//if (Input::IsKey(DIK_UP))
	//{
	//	//上へ移動
	//	_position += UV;
	//}

	////↓が押されていたら
	//if (Input::IsKey(DIK_DOWN))
	//{
	//	//下へ移動
	//	_position -= UV;
	//}

	//→が押されていたら
	if (Input::IsKey(DIK_D))
	{
		//右へ移動
		_position.x += 0.1f;

	}

	//←が押されていたら
	if (Input::IsKey(DIK_A))
	{
		//左へ移動
		_position.x -= 0.1f;
	}

	//↑が押されていたら
	if (Input::IsKey(DIK_W))
	{
		//上へ移動
		_position.z += 0.1f;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_S))
	{
		//下へ移動
		_position.z -= 0.1f;
	}

	
	srand((unsigned int)time(0));
	int move = rand() % 4;

	switch (move)
	{
	case 0: _position.x += 0.1f; break;
	case 1: _position.x -= 0.1f; break;
	case 2: _position.z += 0.1f; break;
	case 3: _position.z -= 0.1f; break;
	}
	
	
}

void Enemy::move()
{
	if (frame_ == 0 || frame_ < 120)
	{
		_rotate.x += 30.0f;
		frame_++;
	}

	if (frame_ >= 120)
	{
		_rotate.y += 30.0f;
		frame_++;
	}

	if (frame_ == 240)
	{
		frame_ = 0;
	}
}

//描画
void Enemy::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "Bullet")
	{
		Hp_--;
		//相手（弾）を消す
		//pTarget->KillMe();
		if (Hp_ == 0)
		{
			//相手（弾）を消す
			//KillMe();
		}
		//ゲームオーバーシーンへ
		/*SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);*/
	}
}