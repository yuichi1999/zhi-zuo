#pragma once
#include "Engine/GameObject/GameObject.h"

//弾を管理するクラス
class Bullet : public IGameObject
{
	int _hModel;		//モデル番号
	D3DXVECTOR3 _move;	//移動ベクトル
	float _dy;			//Ｙ方向の加速度

	const float SPEED;	//速度

	
public:
	Bullet(IGameObject* parent);
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//発射
	//引数；position	発射位置
	//引数：direction	発射方向
	void Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction);
};