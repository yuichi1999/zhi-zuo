#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/global.h"

//◆◆◆を管理するクラス
class EnemyHead : public IGameObject
{
	int _hModel;    //モデル番号
public:
    //コンストラクタ
    EnemyHead(IGameObject* parent);

    //デストラクタ
    ~EnemyHead();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

	//弾を撃つ
	void Shot();
};