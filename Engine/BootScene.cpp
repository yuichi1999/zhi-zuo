#include "bootScene.h"
#include "Engine/global.h"
#include "Stage.h"
#include "Player.h"
#include "Enemy.h"
#include "Engine/ResouceManager/Image.h"
#include <time.h>
#include "Enchant.h"


BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene"), _pText(nullptr), _pText2(nullptr), _pText3(nullptr), frame_(0), cnt1_(0), cnt10_(0), minCnt_(MIN_COUNT)
{
}

void BootScene::Initialize()
{
	//アイテムのリスポーン位置３か所
	spawnPos_[0].EnchantPos.x = 3;
	spawnPos_[0].EnchantPos.y = 0.25;
	spawnPos_[0].EnchantPos.z = 7;

	spawnPos_[1].EnchantPos.x = 7;
	spawnPos_[1].EnchantPos.y = 0.25;
	spawnPos_[1].EnchantPos.z = 7;

	spawnPos_[2].EnchantPos.x = 9;
	spawnPos_[2].EnchantPos.y = 0.25;
	spawnPos_[2].EnchantPos.z = 7;

	//ランダムで1,2,3だす
	srand((unsigned int)time(0));
	int spawn = rand() % 3;

	//アイテムを登場させる
	pEnchant_ = CreateGameObject<Enchant>(this);
	pEnchant_->SetPosition(spawnPos_[spawn].EnchantPos);

	//テキストを作成
	_pText = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText2 = new Text("ＭＳ ゴシック", 60);
	//テキストを作成
	_pText3 = new Text("ＭＳ ゴシック", 60);

	CreateGameObject<Stage>(this);
	CreateGameObject<Player>(this);
	CreateGameObject<Enemy>(this);

	InitTime();
}

void BootScene::Update()
{
	TimeCount();

	if (min_[minCnt_] == "0" && sec_[cnt10_] == "0" && sec_[cnt1_] == "0")
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

void BootScene::Draw()
{
	_pText->Draw(1200, 50, sec_[cnt1_]); //自動で2:59にする
	_pText2->Draw(1160, 50, sec_[cnt10_]); //自動で2:59にする
	_pText3->Draw(1120, 50, min_[minCnt_]); //自動で2:59にする
	//_pText->Draw(100, 100, "これはテスト用のシーンです。");
}

void BootScene::Release()
{
	delete _pText;
	delete _pText2;
	delete _pText3;
}

void BootScene::InitTime()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

void BootScene::TimeCount()
{

	//フレームのカウントが60でちょうど1秒
	frame_++;
	
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初300から299にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 1;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 1;
					cnt1_ = 1;
					minCnt_++;
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

			}
			/*if (minCnt_ == 10 && cnt10_ == 10 && cnt1_ == 10)
			{
			cnt1_ = 0;
			cnt10_ = 0;
			minCnt_ = 7;
			}*/
		}
	}
}
