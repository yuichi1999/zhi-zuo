#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"
#include <stdio.h>
#include <string>

#define MIN_COUNT 7	

class Enchant;
class Player;

class BootScene : public IGameObject 
{
	Text* _pText;    //テキスト
	Text* _pText2;    //テキスト
	Text* _pText3;    //テキスト

	Enchant* pEnchant_;

	Player* pPlayer;

	int _hImage[5];    //画像番号

	char *sec_[11];

	char *min_[11];

	//フレームをカウントする変数
	int frame_;

	//１のくらいのカウント用
	int cnt1_;

	//１０のくらいのカウント用
	int cnt10_;

	//分カウント配列の添字用
	int minCnt_;

	//int sec = 200;

	typedef struct
	{
		D3DXVECTOR3 EnchantPos;
	}SpawnPos;

	//スポーンポイント用配列
	SpawnPos spawnPos_[3];

public:

	BootScene(IGameObject* parent);
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
	void InitTime();
	void TimeCount();
};
