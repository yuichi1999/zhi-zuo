#include "Bullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"), _hModel(-1),_move(D3DXVECTOR3(0, 0, 0)), _dy(0.0f),
	SPEED(0.5f)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Bullet.fbx");
	assert(_hModel >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	//移動
	_position += _move;
	_move.y += _dy;
	_dy -= 0.001f;

	//ある程度低い位置まで落ちたら消す
	if (_position.y < -1)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetMatrix(_hModel, _localMatrix);
	Model::Draw(_hModel);
}

//開放
void Bullet::Release()
{
	Model::Release(_hModel);
}

//発射
void Bullet::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	_position = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	D3DXVec3Normalize(&_move, &direction);
	_move *= SPEED;
}