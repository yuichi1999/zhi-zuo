#include "EnemyStatus.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
EnemyStatus::EnemyStatus(IGameObject * parent)
	:IGameObject(parent, "EnemyStatus"), _hModel(-1)
{
}

//デストラクタ
EnemyStatus::~EnemyStatus()
{
}

//初期化
void EnemyStatus::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Models/Hp5.fbx");
	assert(_hModel >= 0);

	//_position.y -= 3;
	_position.z -= 7;
}

//更新
void EnemyStatus::Update()
{
}

//描画
void EnemyStatus::Draw()
{
	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 0.2f, 0.2f, 1.0f);

	Model::SetMatrix(_hModel, _worldMatrix * a);
	Model::Draw(_hModel);
}

//開放
void EnemyStatus::Release()
{
}